package com.cc.dy.http.impl;

import com.cc.dy.api.request.pay.CreateOrderRequest;
import com.cc.dy.api.response.DyResponse;
import com.cc.dy.api.response.pay.CreateOrderResponse;
import com.cc.dy.config.DyConfig;
import com.cc.dy.executer.DyService;
import com.cc.dy.executer.impl.DefaultDyServiceImpl;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class OkHttpDyHttpClientTest {
    @Test
    void postWithHeaders() {
        DyConfig dyConfig = new DyConfig();
        dyConfig.setAppId("111");

        DyService dyService = new DefaultDyServiceImpl();
        dyService.addDyConfig(dyConfig);

        dyService.switchClient("111");

        OkHttpDyHttpClient apacheDyHttpClient = new OkHttpDyHttpClient(dyService);
        String url = "https://developer.toutiao.com/api/apps/ecpay/v1/create_order";
        String params = "{\"out_order_no\":\"11121\",\"total_amount\":1,\"subject\":\"测试商品\",\"body\":\"测试商品\",\"valid_time\":150,\"notify_url\":\"https://movie.cetregistrationnumber.top/pay-center/pay/tiktok/callback\",\"app_id\":\"tt43d43569dceb1f9f01\",\"sign\":\"884a514d8c8d52c14dbe0b0c7e4cda85\"}";
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("access_token", "0801121846344c614f7937655342584b4b4a445477792f4768513d3d1");
        headerMap.put("Content-Type", "application/json");

        CreateOrderRequest request = new CreateOrderRequest();
        request.setOutOrderNo("11121");
        request.setTotalAmount(1L);
        request.setSubject("测试商品");
        request.setBody("测试商品");
        request.setValidTime(150L);
//        request.setCpExtra("test");
        request.setNotifyUrl("https://movie.cetregistrationnumber.top/pay-center/pay/tiktok/callback");
        request.setAppId("tt43d43569dceb1f9f01");
        request.setSign("884a514d8c8d52c14dbe0b0c7e4cda851");

//        CreateOrderResponse createOrderResponse = dyService.getDyPayService().createOrder(request);

        DyResponse response = apacheDyHttpClient.postWithHeaders(url, headerMap, request, CreateOrderResponse.class);
        System.out.println(response);
    }
}