package com.cc.dy.http.impl;

import com.cc.dy.api.response.DyResponse;
import com.cc.dy.api.response.pay.CreateOrderResponse;
import com.cc.dy.config.DyConfig;
import com.cc.dy.executer.DyService;
import com.cc.dy.executer.impl.DefaultDyServiceImpl;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class ApacheDyHttpClientTest {

    @Test
    void postWithHeaders() {
        DyConfig dyConfig = new DyConfig();
        dyConfig.setAppId("111");

        DyService dyService = new DefaultDyServiceImpl();
        dyService.addDyConfig(dyConfig);

        dyService.switchClient("111");

        ApacheDyHttpClient apacheDyHttpClient = new ApacheDyHttpClient(dyService);
        String url = "https://developer.toutiao.com/api/apps/ecpay/v1/create_order";
        String params = "{\"out_order_no\":\"11121\",\"total_amount\":1,\"subject\":\"测试商品\",\"body\":\"测试商品\",\"valid_time\":150,\"notify_url\":\"https://movie.cetregistrationnumber.top/pay-center/pay/tiktok/callback\",\"app_id\":\"tt43d43569dceb1f9f01\",\"sign\":\"884a514d8c8d52c14dbe0b0c7e4cda85\"}";
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("access_token", "0801121846344c48786d7a756f34626755592f786237627838413d3d");
        headerMap.put("Content-Type", "application/json");
        DyResponse dyResponse = apacheDyHttpClient.postWithHeaders(url, headerMap, params, CreateOrderResponse.class);
        System.out.println(dyResponse);
    }
}