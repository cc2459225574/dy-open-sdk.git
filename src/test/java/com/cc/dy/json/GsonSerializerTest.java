package com.cc.dy.json;

import com.cc.dy.api.response.DyResponse;
import com.cc.dy.api.response.auth.DyAccessTokenResponseV2;
import com.cc.dy.api.response.auth.DyJsCode2sessionResponse;
import com.cc.dy.api.response.pay.CreateOrderResponse;
import com.cc.dy.api.response.pay.QueryOrderResponse;
import com.cc.dy.json.impl.FastJsonSerializer;
import org.junit.jupiter.api.Test;

class GsonSerializerTest {

    @Test
    void parseResponse() {
        String ss = "{\"err_no\":0,\"err_tips\":\"success\",\"data\":{\"order_id\":\"N7276380432999008512\",\"order_token\":\"CgwIARDGJRiuMiABKAESTgpM9KLFZ+2HECOil8GtC0nO4t6U/jWWhgX/XOcpKOJvTXMMRx97DfkB1BWtR7YmceHmXtVewv7lkucWD4hZzOZfBnSlbqRCNfGqf2X3vBoA.L\",\"url\":\"\"}}";
        DyResponse dyResponse = new FastJsonSerializer().parseResponse(ss, CreateOrderResponse.class);
        System.out.println(dyResponse);
    }

    @Test
    void parseCreateOrderResponse() {
        String response = "{\"err_no\":0,\"err_tips\":\"success\",\"data\":{\"order_id\":\"N7291881073338763557\",\"order_token\":\"CgYIASAHKAESTgpMfw8ujOG+fYCAp7T8WaJcB2Ilno5nqLM1nhSvIuz+Vu5lsO5rMqZ4++l4C/bYfMWLC8QsRWsdHhU+Atk3AAV5tCOg2ch4EkodVzTYUBoA.L\",\"url\":\"\"}}";
        CreateOrderResponse parse = new FastJsonSerializer().parse(response, CreateOrderResponse.class);
        System.out.println(parse);
        CreateOrderResponse createOrderResponse = new FastJsonSerializer().parseResponse(response, CreateOrderResponse.class);
        System.out.println(createOrderResponse);
    }

    @Test
    void parseAccessTokenResponse() {
        String response = "{\"err_no\":0,\"err_tips\":\"success\",\"data\":{\"access_token\":\"0801121846342b3871745a57746a754b336333685973486245513d3d\",\"expires_in\":7200,\"expiresAt\":1697790226}}";
        DyAccessTokenResponseV2 parse = new FastJsonSerializer().parseResponse(response, DyAccessTokenResponseV2.class);
        System.out.println(parse);
    }

    @Test
    void parseDyJsCode2sessionResponse() {
        String response = "{\"err_no\":0,\"err_tips\":\"success\",\"data\":{\"session_key\":\"IC3lQIz5Inr1BaSS9JPTbw==\",\"openid\":\"_000ArvONJlNH7ST10xZv7Y2seI62AVzy7eG\",\"anonymous_openid\":\"\",\"unionid\":\"19db9343-de3f-4bf6-9998-e238643c3ad8\",\"dopenid\":\"\"}}";
        DyJsCode2sessionResponse parse = new FastJsonSerializer().parseResponse(response, DyJsCode2sessionResponse.class);
        System.out.println(parse);
    }

    @Test
    void parseQueryOrderResponse() {
        String response = "{\"err_no\":0,\"err_tips\":\"\",\"out_order_no\":\"11110112\",\"order_id\":\"N7291966142925506854\",\"payment_info\":{\"total_fee\":1,\"order_status\":\"TIMEOUT\",\"pay_time\":\"\",\"way\":0,\"channel_no\":\"\",\"channel_gateway_no\":\"\",\"seller_uid\":\"72247523577944824930\",\"item_id\":\"\",\"cp_extra\":\"\",\"msg\":\"\",\"author_id\":0},\"cps_info\":\"\"}";
        QueryOrderResponse parse = new FastJsonSerializer().parseResponse(response, QueryOrderResponse.class);
        System.out.println(parse);
    }
}