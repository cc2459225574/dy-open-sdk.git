package com.cc.dy.constant;

public interface DyApiUrlConstants {

    /**
     * 登录
     */
    interface Auth {
        /**
         * code2Session
         */
        String CODE2SESSION = "/api/apps/v2/jscode2session";
        /**
         * getAccessToken
         */
        String ACCESS_TOKEN_V2 = "/api/apps/v2/token";
        /**
         * 刷新授权调用凭证
         */
        String RENEW_REFRESH_TOKEN = "/oauth/renew_refresh_token";
        /**
         * 刷新用户授权调用凭证
         */
        String REFRESH_TOKEN = "/oauth/refresh_token";
        /**
         * 获取用户授权调用凭证
         */
        String ACCESS_TOKEN = "/oauth/access_token";
    }

    interface UrlLink {
        /**
         * 生成SchemaV2
         */
        String GENERATE_SCHEMA_V2 = "/api/apps/v1/url/generate_schema";
        /**
         * 查询SchemaV2
         */
        String QUERY_SCHEMA_V2 = "/api/apps/v1/url/query_schema";
        /**
         * 查询Schema配额V2
         */
        String QUERY_SCHEMA_QUOTA_V2 = "/api/apps/v1/url/query_schema_quota";
        /**
         * 生成 Link
         */
        String GENERATE_LINK = "/api/apps/url_link/generate";
        /**
         * 生成 LinkV2
         */
        String GENERATE_LINK_V2 = "/api/apps/v1/url_link/generate";
        /**
         * 查询 Link 配额V2
         */
        String QUERY_LINK_QUOTA_V2 = "/api/apps/v1/url_link/query_quota";
        /**
         * 查询 Link V2
         */
        String QUERY_LINK_INFO_V2 = "/api/apps/v1/url_link/query_info";
        /**
         * createQRCodeV2
         */
        String CREATE_QRCODE_V2 = "/api/apps/v1/qrcode/create";
    }

    interface Im {
        /**
         * createQRCodeV2
         */
        String SEND_MSG = "/im/authorize/send/msg";
    }

    interface UserUrl {
        String USER_INFO = "/oauth/userinfo";
    }

    interface Pay {
        /**
         * 预下单接口
         */
        String CREATE_ORDER = "/api/apps/ecpay/v1/create_order";
        /**
         * 支付结果查询
         */
        String QUERY_ORDER = "/api/apps/ecpay/v1/query_order";
        /**
         * 发起退款
         */
        String CREATE_REFUND = "/api/apps/ecpay/v1/create_refund";
        /**
         * 退款结果查询
         */
        String QUERY_REFUND = "/api/apps/ecpay/v1/query_refund";
        /**
         * 发起退分账
         */
        String CREATE_RETURN = "/api/apps/ecpay/v1/create_return";
    }
}
