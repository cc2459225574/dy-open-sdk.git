package com.cc.dy.constant;

public interface DyConstants {
    /**
     * ACCESS_TOKEN缓存前缀
     */
    String ACCESS_TOKEN_CACHE_KEY = "DY:ACCESS_TOKEN:";

    /**
     * 获取 access_token 时值为 client_credential
     */
    String ACCESS_TOKEN_GRANT_TYPE = "client_credential";

    /**
     * ACCESS_TOKEN缓存时间
     */
    Integer ACCESS_TOKEN_EXPIRE_TIME = 7200;

    /**
     * 请求头access_token key
     */
    String ACCESS_TOKEN_HEADER_KEY = "access_token";
}
