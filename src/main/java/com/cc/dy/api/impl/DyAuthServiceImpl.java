package com.cc.dy.api.impl;

import cn.hutool.core.lang.Assert;
import com.cc.dy.api.AbstractApiService;
import com.cc.dy.api.DyAuthService;
import com.cc.dy.api.request.auth.DyAccessTokenRequest;
import com.cc.dy.api.request.auth.DyAccessTokenRequestV2;
import com.cc.dy.api.request.auth.DyClientTokenRequest;
import com.cc.dy.api.request.auth.DyJsCode2sessionRequest;
import com.cc.dy.api.request.auth.DyRefreshTokenRequest;
import com.cc.dy.api.request.auth.DyRenewRefreshTokenRequest;
import com.cc.dy.api.response.auth.DyAccessTokenResponse;
import com.cc.dy.api.response.auth.DyAccessTokenResponseV2;
import com.cc.dy.api.response.auth.DyClientTokenResponse;
import com.cc.dy.api.response.auth.DyJsCode2sessionResponse;
import com.cc.dy.api.response.auth.DyRefreshTokenResponse;
import com.cc.dy.api.response.auth.DyRenewRefreshTokenResponse;
import com.cc.dy.config.DyConfig;
import com.cc.dy.constant.DyApiUrlConstants;
import com.cc.dy.constant.DyConstants;
import com.cc.dy.enums.DyErrorCode;
import com.cc.dy.executer.DyService;
import org.apache.commons.lang3.StringUtils;

import java.util.concurrent.TimeUnit;

public class DyAuthServiceImpl extends AbstractApiService implements DyAuthService {

    public DyAuthServiceImpl(DyService dyService) {
        super(dyService);
    }

    /**
     * 该接口用于获取接口调用的凭证 client_token。该接口适用于抖音授权；
     * 后续小程序各 OpenAPI 会逐步推出和 client_token 相适配的各新接口，原旧接口使用 getAccessToken 的方式依然维护；
     * 各 OpenAPI 使用 token 的类型需要参考接口文档里 token 参数的说明；
     * 原有开发者依然可以使用旧接口，但建议迁移；
     * 接口说明
     * 业务场景
     * client_token 用于不需要用户授权就可以调用的接口。
     * 注意事项
     * client_token 的有效时间为 2 个小时，重复获取 client_token 后会使上次的 client_token 失效（但有 5 分钟的缓冲时间，连续多次获取 client_token 只会保留最新的两个 client_token）。
     * 请求地址
     * POST https://open.douyin.com/oauth/client_token/
     * 沙盒地址
     * POST https://open-sandbox.douyin.com/oauth/client_token/
     * 请求示例
     * curl --location --request POST 'https://open.douyin.com/oauth/client_token/' \
     * --header 'Content-Type: multipart/form-data' \
     * --form 'grant_type="client_credential"' \
     * --form 'client_key="tt******"' \
     * --form 'client_secret="7802f4e6f243e659d51135445fe*****"'
     * 正常示例
     * {
     * "data": {
     * "access_token": "clt.75c380db41e815978a733994d96f5d23RqilUxH48iobyWhbIOQFo******",
     * "description": "",
     * "error_code": 0,
     * "expires_in": "7200"
     * },
     * "message": "success"
     * }
     * 异常样例
     * {
     * "data": {
     * "description": "Parameter error",
     * "error_code": 2100005
     * },
     * "extra": {
     * "logid": "2020070614111601022506808001045D59",
     * "now": 1594015876138
     * }
     * }
     */
    @Override
    public DyClientTokenResponse clientToken(DyClientTokenRequest dyClienttokenRequest) {
        DyConfig dyConfig = dyService.getDyConfig();
        Assert.notNull(dyConfig, DyErrorCode.ERROR_CONFIG_NOT_EXIST);
        dyClienttokenRequest.setClientKey(dyConfig.getAppId());
        dyClienttokenRequest.setClientSecret(dyConfig.getClientSecret());
        return post(DyApiUrlConstants.Auth.ACCESS_TOKEN, dyClienttokenRequest, DyClientTokenResponse.class);
    }

    /**
     * 该接口用于获取用户授权第三方接口调用的凭证 access_token；该接口适用于抖音授权。
     * 接口说明
     * 业务场景
     * access_token 为用户授权第三方接口调用的凭证，存储在客户端，可能会被窃取，泄漏后可能会发生用户隐私数据泄漏的风险，建议存储在服务端。
     * 注意事项
     * 获取到 access_token 后授权临时票据 (code) 不要再授权刷新，否则会导致上一次获取的 code 过期。
     * 调用该接口前，需要通过tt.showDouyinOpenAuth获取用户授权码。
     * 权限要求
     * 无
     * 请求示例
     * curl --location 'https://open.douyin.com/oauth/access_token/' \
     * --header 'Content-Type: application/json' \
     * --data '{
     * "grant_type": "authorization_code",
     * "client_key": "tt10abc****",
     * "client_secret": "7802f4e6f243e659d51135445fe******",
     * "code": "ffab5ec26cd958fditn2GNr8Wx5m0i******"
     * }'
     * 正常示例
     * {
     * "data": {
     * "access_token": "act.f7094fbffab2ecbfc45e9af9c32bc241oYdckvBKe82BPx8T******",
     * "captcha": "",
     * "desc_url": "",
     * "description": "",
     * "error_code": 0,
     * "expires_in": 1296000,
     * "log_id": "20230525105733ED3ED7AC56A******",
     * "open_id": "b9b71865-7fea-44cc-******",
     * "refresh_expires_in": 2592000,
     * "refresh_token": "rft.713900b74edde9f30ec4e246b706da30t******",
     * "scope": "user_info"
     * },
     * "message": "success"
     * }
     * 异常示例
     * {
     * "data": {
     * "description": "Parameter error",
     * "error_code": 2100005
     * },
     * "extra": {
     * "logid": "2020070614111601022506808001045D59",
     * "now": 1594015876138
     * }
     * }
     */
    @Override
    public DyAccessTokenResponse accessToken(DyAccessTokenRequest dyAccessTokenRequest) {
        DyConfig dyConfig = dyService.getDyConfig();
        Assert.notNull(dyConfig, DyErrorCode.ERROR_CONFIG_NOT_EXIST);
        dyAccessTokenRequest.setClientKey(dyConfig.getAppId());
        dyAccessTokenRequest.setClientSecret(dyConfig.getClientSecret());
        return post(DyApiUrlConstants.Auth.ACCESS_TOKEN, dyAccessTokenRequest, DyAccessTokenResponse.class);
    }

    /**
     * 该接口用于刷新 access_token 的有效期；该接口适用于抖音授权。
     * 接口说明
     * access_token 有效期说明
     * 当 access_token 过期（过期时间 15 天）后，可以通过该接口使用 refresh_token（过期时间 30 天）进行刷新。刷新后获得一个有效期为15天的 access_token，但是 refresh_token 的有效期保持不变。
     * 若 refresh_token 过期，获取 access_token 会报错（error_code=10010），此时需要重新引导用户授权。
     * 用户可以在抖音-我-设置（右上角）-帐号与安全-授权管理 中取消对应用的授权，取消授权后原有 access_token 会立即失效。
     * 抖音开放平台会定期对用户授权进行检查，取消不合规的 access_token 授权。
     * 请求地址
     * HTTP URL
     * 正式环境：https://open.douyin.com/oauth/refresh_token/
     * 沙盒环境：https://open-sandbox.douyin.com/oauth/refresh_token/
     * 权限要求
     * 无
     * 请求示例
     * curl --location --request POST 'https://open.douyin.com/oauth/refresh_token/' \
     * --header 'Content-Type: application/x-www-form-urlencoded' \
     * --data-urlencode 'client_key=tt10abc****' \
     * --data-urlencode 'grant_type=refresh_token' \
     * --data-urlencode 'refresh_token=rft.a736b70544519999a623d67******'
     * 正常示例
     * {
     * "data": {
     * "access_token": "access_token",
     * "description": "",
     * "error_code": 0,
     * "expires_in": "86400",
     * "open_id": "SuQB*******KN5f",
     * "refresh_expires_in": "86400",
     * "refresh_token": "refresh_token",
     * "scope": "user_info"
     * },
     * "message": "success"
     * }
     * 异常样例
     * {
     * "data": {
     * "description": "Parameter error",
     * "error_code": 2100005
     * },
     * "extra": {
     * "logid": "2020070614111601022506808001045D59",
     * "now": 1594015876138
     * }
     * }
     */
    @Override
    public DyRefreshTokenResponse refreshToken(DyRefreshTokenRequest dyRefreshTokenRequest) {
        return post(DyApiUrlConstants.Auth.REFRESH_TOKEN, dyRefreshTokenRequest, DyRefreshTokenResponse.class);
    }

    /**
     * 该接口用于刷新 refresh_token 的有效期。该接口适用于抖音授权。
     * 接口说明
     * 注意事项
     * 抖音的 OAuth API 以https://open.douyin.com/ 开头。
     * 刷新操作需要在 refresh_token 过期前进行。
     * 通过旧的 refresh_token 获取新的 refresh_token，调用后旧 refresh_token 会失效，新 refresh_token 有 30 天有效期。最多只能获取 5 次新的 refresh_token，5 次过后需要用户重新授权。
     * 请求地址
     * HTTP URL
     * 正式环境：https://open.douyin.com/oauth/renew_refresh_token/
     * 沙盒环境：https://open-sandbox.douyin.com/oauth/renew_refresh_token/
     * 权限要求
     * 无
     * 请求示例
     * curl --location --request POST 'https://open.douyin.com/oauth/renew_refresh_token/' \
     * --header 'Content-Type: application/x-www-form-urlencoded' \
     * --data-urlencode 'client_key=tt10abc******' \
     * --data-urlencode 'refresh_token=rft.a736b70544519999a6******'
     * 正常示例
     * {
     * "data": {
     * "description": "",
     * "error_code": 0,
     * "expires_in": "86400",
     * "refresh_token": "refresh_token"
     * },
     * "message": "success"
     * }
     * 异常示例
     * {
     * "data": {
     * "description": "Parameter error",
     * "error_code": 2100005
     * },
     * "extra": {
     * "logid": "2020070614111601022506808001045D59",
     * "now": 1594015876138
     * }
     * }
     */
    @Override
    public DyRenewRefreshTokenResponse renewRefreshToken(DyRenewRefreshTokenRequest dyRenewRefreshTokenRequest) {
        return post(DyApiUrlConstants.Auth.RENEW_REFRESH_TOKEN, dyRenewRefreshTokenRequest, DyRenewRefreshTokenResponse.class);
    }

    /**
     * 获取AccessToken
     */
    @Override
    public synchronized String getAccessToken() {
        DyConfig dyConfig = this.dyService.getDyConfig();
        Assert.notNull(dyConfig, DyErrorCode.ERROR_CONFIG_NOT_EXIST);
        String accessToken = this.dyService.getStorageService().getValue(DyConstants.ACCESS_TOKEN_CACHE_KEY + dyConfig.getAppId());
        if (StringUtils.isBlank(accessToken)) {
            DyAccessTokenResponseV2 dyAccessTokenResponseV2 = getAccessToken(new DyAccessTokenRequestV2());
            accessToken = dyAccessTokenResponseV2.getAccessToken();
            this.dyService.getStorageService().setValue(DyConstants.ACCESS_TOKEN_CACHE_KEY + dyConfig.getAppId(), accessToken, DyConstants.ACCESS_TOKEN_EXPIRE_TIME, TimeUnit.SECONDS);
        }
        return accessToken;
    }

    /**
     * 为了保障应用的数据安全，只能在开发者服务器使用 AppSecret，如果小程序存在泄露 AppSecret 的问题，字节小程序平台将有可能下架该小程序，并暂停该小程序相关服务。
     * access_token 是小程序的全局唯一调用凭据，开发者调用小程序支付时需要使用 access_token。access_token 的有效期为 2 个小时，需要定时刷新 access_token，重复获取会导致之前一次获取的 access_token 的有效期缩短为 5 分钟。
     * 后续会新增和抖音能力 client_token 相通的接口，各 openapi 支持 client_token 调用，建议开发者逐步迁移新接口。
     * 请求地址
     * 线上地址
     * POST https://developer.toutiao.com/api/apps/v2/token
     * 沙盒地址
     * POST https://open-sandbox.douyin.com/api/apps/v2/token
     * 请求示例
     * POST 请求 body
     * {
     * "appid": "ttabc*****",
     * "secret": "d428***********",
     * "grant_type": "client_credential"
     * }
     * 返回示例
     * 正常返回
     * {
     * "err_no": 0,
     * "err_tips": "success",
     * "data": {
     * "access_token": "0801121***********",
     * "expires_in": 7200
     * }
     * }
     * 错误返回
     * {
     * "err_no": 40017,
     * "err_tips": "bad secret",
     * "data": {
     * "access_token": "",
     * "expires_in": 0
     * }
     * }
     * Bug & Tip
     * Tip: token 是小程序级别 token，不要为每个用户单独分配一个 token，会导致 token 校验失败。建议每小时更新一次即可。
     */
    @Override
    public DyAccessTokenResponseV2 getAccessToken(DyAccessTokenRequestV2 dyAccessTokenRequestV2) {
        DyConfig dyConfig = dyService.getDyConfig();
        Assert.notNull(dyConfig, DyErrorCode.ERROR_CONFIG_NOT_EXIST);
        dyAccessTokenRequestV2.setAppid(dyConfig.getAppId());
        dyAccessTokenRequestV2.setSecret(dyConfig.getClientSecret());
        return post(DyApiUrlConstants.Auth.ACCESS_TOKEN_V2, dyAccessTokenRequestV2, DyAccessTokenResponseV2.class);
    }

    /**
     * 为了保障应用的数据安全，只能在开发者服务器使用 AppSecret；开发者服务器不应该把会话密钥下发到小程序，也不应该对外提供这个密钥。如果小程序存在泄露 AppSecret 或会话密钥的问题，字节小程序平台将有可能下架该小程序，并暂停该小程序相关服务。
     * 通过login接口获取到登录凭证后，开发者可以通过服务器发送请求的方式获取 session_key 和 openid。
     * Tip：登录凭证 code，anonymous_code 只能使用一次，非匿名需要 code，非匿名下的 anonymous_code 用于数据同步，匿名需要 anonymous_code。
     * 请求地址
     * 线上地址
     * POST https://developer.toutiao.com/api/apps/v2/jscode2session
     * 沙盒地址
     * POST https://open-sandbox.douyin.com/api/apps/v2/jscode2session
     * 请求示例
     * POST 请求 body
     * {
     * "appid": "ttabc****",
     * "secret": "d428**************7",
     * "anonymous_code": "",
     * "code": "iOyVA5hc*******"
     * }
     * 返回示例
     * 正确返回
     * {
     * "err_no": 0,
     * "err_tips": "success",
     * "data": {
     * "session_key": "hZy6t19VPjFqm********",
     * "openid": "V3WvSshYq9******",
     * "anonymous_openid": "",
     * "unionid": "f7510d9ab***********"
     * }
     * }
     * 错误返回
     * {
     * "err_no": 40015,
     * "err_tips": "bad appid",
     * "data": {
     * "session_key": "",
     * "openid": "",
     * "anonymous_openid": "",
     * "unionid": ""
     * }
     * }
     * QA
     * Q：调用 code2Session 接口，返回 bad code。如何处理？
     * A：调用 tt.login 获取 code 时所用的 appId，和调用 code2Session 接口所传的 appid 不一致。
     */
    @Override
    public DyJsCode2sessionResponse jsCode2session(DyJsCode2sessionRequest dyJsCode2sessionRequest) {
        DyConfig dyConfig = dyService.getDyConfig();
        Assert.notNull(dyConfig, DyErrorCode.ERROR_CONFIG_NOT_EXIST);
        dyJsCode2sessionRequest.setAppid(dyConfig.getAppId());
        dyJsCode2sessionRequest.setSecret(dyConfig.getClientSecret());
        return post(DyApiUrlConstants.Auth.CODE2SESSION, dyJsCode2sessionRequest, DyJsCode2sessionResponse.class);
    }
}
