package com.cc.dy.api.impl;

import com.cc.dy.api.AbstractApiService;
import com.cc.dy.api.DyImService;
import com.cc.dy.api.request.im.DyClientUploadRequest;
import com.cc.dy.api.request.im.DyDelAppletTemplateRequest;
import com.cc.dy.api.request.im.DyGetAppletTemplateRequest;
import com.cc.dy.api.request.im.DySendMsgRequest;
import com.cc.dy.api.request.im.DySetAppletTemplateRequest;
import com.cc.dy.api.response.im.DyClientUploadResponse;
import com.cc.dy.api.response.im.DyDelAppletTemplateResponse;
import com.cc.dy.api.response.im.DyGetAppletTemplateResponse;
import com.cc.dy.api.response.im.DySendMsgResponse;
import com.cc.dy.api.response.im.DySetAppletTemplateResponse;
import com.cc.dy.constant.DyApiUrlConstants;
import com.cc.dy.executer.DyService;

import java.util.HashMap;
import java.util.Map;

/**
 * @author chenChao 2023-09-15 18:12
 */
public class DyImServiceImpl extends AbstractApiService implements DyImService {
    public DyImServiceImpl(DyService dyService) {
        super(dyService);
    }

    /**
     * Scope:   im.authorize_message.admin  需要申请权限  需要用户授权 (通过tt.showDouyinOpenAuth)
     * 使用限制
     * 普通用户每天可以主动授权给服务账号2次；
     * 每授权一次最多可以发送3条消息；
     * 如果只授权一次，且一直没有调用该接口，则有30天的有效期。
     * 接口说明
     * 该能力需申请权限使用，满足申请条件后，开发者需登录控制台，在小程序应用中筛选“能力-能力实验室-小程序能力”，申请小程序私信/群管理能力用于私信发送。 注意，需要提前在能力实验室中申请主动授权私信能力（button组件）用于私信授权获取。
     * 小程序私信/群管理能力开通后，默认开通im.authorize_message.admin，im.microapp_card，tool.image.upload scope权限；
     * 支持发送文本、图片、视频和小程序卡片
     * 视频需要通过 video.list.bind 单独请求授权，视频只能发布自己公开视频
     * 权限要求
     * 经营者可在“控制台-能力-能力实验室”中申请 小程序私信/群管理
     * 请求示例
     * curl -X POST 'https://open.douyin.com/im/authorize/send/msg/?open_id=aa-aa-aa' \
     * -H 'Content-Type:application/json' \
     * -H 'access-token:act.1d1021d2aee3d41fee2d2add43456badMFZnrhFhfWotu3Ec' \
     * --data '{
     * "content": {
     * "msg_type": 2,
     * "image": {
     * "media_id": "@aaaa"
     * }
     * },
     * "to_user_id": "bb-bb-xx"
     * }'
     * 正常示例
     * {
     * "err_msg":"",
     * "err_no":0,
     * "data":{
     * "msg_id":"@c29bfc000+MNggFhRkwGuX1ntuc0RPc0VHKefj/yWEJ8DtjU=="
     * },
     * "log_id":"11111"
     * }
     */
    @Override
    public DySendMsgResponse sendMsg(DySendMsgRequest dySendMsgRequest) {
        Map<String, Object> urlParamMap = new HashMap<>();
        urlParamMap.put("open_id", dySendMsgRequest.getOpenId());
        return postWithAccessToken(DyApiUrlConstants.Im.SEND_MSG, urlParamMap, dySendMsgRequest, DySendMsgResponse.class);
    }

    /**
     * Scope: im.microapp_card 需要申请权限 不需要用户授权
     * 使用限制
     * 同一小程序，可以配置多张小程序引导卡片模板，调用限制为近 30 天可创建 / 更新小程序引导卡片模板 100 次
     * 接口说明
     * 模板好以后，无法再修改卡片类型 card_type
     * 小程序引导卡片示例
     * 创建好卡片后，可通过发送私信消息接口进行小程序引导卡片发送，content 示例如下
     * "content": {
     * "msg_type": 10,
     * "applet_card": {
     * "card_template_id": "@9VwNxuKKBZ03MXG7M8ooWM771FjUAMW/BqhMlDebEmyyzJD7cZENrR868oDbX9xx",
     * "schema":"https://z.douyin.com/ijBoQwF"
     * }
     * }
     * schema 获取方式
     * 通过接口生成 Generate，通过 expire_time 参数控制过期时间，过期后需重新生成。
     * 在小程序界面点击右上角 ... > 分享 > 复制链接，链接有效期为45天，过期后需重新生成。
     * 接口存在防重复提交机制，应用多次调用接口传入相同的 content, media_id,title 参数创建小程序引导卡片，接口会返回相同的 card_template_id
     * 权限要求
     * 新能力上线能力实验室后，经营者可在“控制台-能力管理-能力实验室”中申请。后期能力若转为正式开放能力，可在“控制台-能力管理-互动管理”中申请。
     * 请求示例
     * curl -X POST 'https://open.douyin.com/api/douyin/v1/im/set_applet_template/' -H 'Content-Type:application/json' -H 'access-token:clt.943da17996fb5cebfbc70c044c3fc25a57T54DcjT6HNKGqnUdxzy1' --data '{"card_type":1,"title":"title测试","content":"content测试","media_id":"@9VwNxuKKBZ03MXG7M8ooWM6+iib0bqafYog16knsa1AUPKajyCbkeFIfvjDAhgpsWGXCimkBdNNS2dWASnfgCXqIi7Z6mT1hatJT3UhT8k963Osx4MrrU60NZwIlVg3j","name":"name测试","icon_media_id":"@9VwNxuKKBZ03MXG7M8ooWM6+iib0bqafYog16knsa1AUPKajyCbkeFIfvjDAhgpsWGXCimkBdNNS2dWASnfgCXqIi7Z6mT1hatJT3UhT8k963Osx4MrrU60NZwIlVg3j"}'
     * 正常示例
     * {
     * "extra": {
     * "now": 1660109739,
     * "logid": "20220810133539010174255181020C5143",
     * "error_code": 0,
     * "description": "",
     * "sub_error_code": 0,
     * "sub_description": ""
     * },
     * "data": {
     * "error_code": 0,
     * "description": "",
     * "card_template_id": "@72MqHzC5kqIEgB56A10R9n1psNsNe8gPbkeqQAHKbHqoQ5jL3za3OAGvMtWtnszA"
     * }
     * }
     * 异常示例
     * {
     * "extra": {
     * "sub_description": "",
     * "sub_error_code": 0,
     * "description": "参数不合法",
     * "error_code": 2100005,
     * "logid": "202203271807199888808121990491456D",
     * "now": 1648375639
     * },
     * "data": {
     * "error_code": 2100005,
     * "description": "参数不合法"
     * }
     * }
     */
    @Override
    public DySetAppletTemplateResponse setAppletTemplate(DySetAppletTemplateRequest dySetAppletTemplateRequest) {
        return null;
    }

    /**
     * Scope: im.microapp_card 需要申请权限 不需要用户授权
     * 使用限制
     * 无
     * 接口说明
     * 无
     * 权限要求
     * 新能力上线能力实验室后，经营者可在“控制台-能力管理-能力实验室”中申请。后期能力若转为正式开放能力，可在“控制台-能力管理-互动管理”中申请。
     * 请求示例
     * curl -X POST 'https://open.douyin.com/api/douyin/v1/im/get_applet_template/' -H 'Content-Type:application/json' -H 'access-token:clt.943da17996fb5cebfbc70c044c3fc25a57T54DcjT6HNKGqnUdxzy1' --data '{"cursor":0,"count":50}'
     * 正常示例
     * {
     * "extra": {
     * "now": 1660109739,
     * "logid": "20220810133539010174255181020C5143",
     * "error_code": 0,
     * "description": "",
     * "sub_error_code": 0,
     * "sub_description": ""
     * },
     * "data": {
     * "error_code": 0,
     * "description": "",
     * "has_more": false,
     * "cursor": 0,
     * "list": [
     * {
     * "status": 1,
     * "title": "邀请你一起打决胜巅峰匹配赛",
     * "content": "点击加入",
     * "card_template_id": "@72MqHzC5kqIEgB56A10R9n1psNsNe8gPbkeqQAHKbHqoQ5jL3za3OAGvMtWtnszA",
     * "card_type": 1,
     * "media_id": "@72MqHzC5kqIEgB56A10R9n1psNsNe8gPbkeqQAHKbHqoQ5jL3za3OAGvMt",
     * "name": "决胜巅峰",
     * "app_icon_url": "https://p3-developer-sign.bytemaimg.com/tos-cn-i-ke512zj2cu/a37b56ae3da84c2b98e4ad3e1ac7a1320~tplv-ke512zj2cu-144x144.jpeg?x-expires=2628437565&x-signature=kRfqYlvF17z%2FYdspB5lHAEQBzQM%3D",
     * "create_time": 1571075129,
     * "update_time": 1571075129
     * }
     * ]
     * }
     * }
     * 异常示例
     * {
     * "extra": {
     * "sub_description": "",
     * "sub_error_code": 0,
     * "description": "参数不合法",
     * "error_code": 2100005,
     * "logid": "202203271807199888808121990491456D",
     * "now": 1648375639
     * },
     * "data": {
     * "error_code": 2100005,
     * "description": "参数不合法"
     * }
     * }
     */
    @Override
    public DyGetAppletTemplateResponse getAppletTemplate(DyGetAppletTemplateRequest dyGetAppletTemplateRequest) {
        return null;
    }

    /**
     * Scope: im.microapp_card 需要申请权限 不需要用户授权
     * 使用限制
     * 无
     * 接口说明
     * 无
     * 权限要求
     * 新能力上线能力实验室后，经营者可在“控制台-能力管理-能力实验室”中申请。后期能力若转为正式开放能力，可在“控制台-能力管理-互动管理”中申请。
     * 请求示例
     * curl -X POST 'https://open.douyin.com/api/douyin/v1/im/del_applet_template/?card_template_id=%409VwWjvGUTp9kKW%2Bma4huFs7961vXA8a8BatPlzSYEW9LXs%2FTnnmopzn9582jQXZQ' -H 'Content-Type:application/json; charset=utf-8' -H 'access-token:clt.943da17996fb5cebfbc70c044c3fc25a57T54DcjT6HNKGqnUdxzy1' --data ''
     * 正常示例
     * {
     * "extra": {
     * "logid": "2022081013405301017425518105798161",
     * "error_code": 0,
     * "description": "",
     * "sub_error_code": 0,
     * "sub_description": "",
     * "now": 1660110053
     * },
     * "data": {
     * "error_code": 0,
     * "description": ""
     * }
     * }
     * 异常示例
     * {
     * "extra": {
     * "sub_description": "",
     * "sub_error_code": 0,
     * "description": "参数不合法",
     * "error_code": 2100005,
     * "logid": "202203271807199888808121990491456D",
     * "now": 1648375639
     * },
     * "data": {
     * "error_code": 2100005,
     * "description": "参数不合法"
     * }
     * }
     */
    @Override
    public DyDelAppletTemplateResponse delAppletTemplate(DyDelAppletTemplateRequest dyDelAppletTemplateRequest) {
        return null;
    }

    /**
     * scope: tool.image.upload 需要申请权限  不需要用户授权
     * 该接口用于上传图片，返回的 image_id 作为建群接口的 avatar_uri 字段传入。
     * 使用限制
     * 图片不超过 10 MB
     * 仅支持 jpeg, png, bmp, webp 图片格式
     * 接口说明
     * 返回的 image_id，可用于 创建/更新留资卡片, 创建更新小程序引导卡片模板 等接口的图片参数
     * 权限要求
     * 新能力上线能力实验室后，经营者可在“控制台-能力管理-能力实验室”中申请。后期能力若转为正式开放能力，可在“控制台-能力管理-互动管理”中申请。
     * 请求示例
     * curl --location 'http://open.douyin.com/tool/imagex/client_upload/' \
     * --header 'access-token: clt.2.PHIvk97aUgSAUZ_Nn7SgbFLuxNH2dxvZaFkJb6IZuu4v8JDO06YwUNX4QpVeMDFUZx_o6sS7kEUokA0vB4ZDmKNV-KzlBfq4ntVSdmT5_RE9tuZWsTPc-9ZZLVYjheQbu7siGOOe4uA-zVSW0YJDZYwVG6eiNbl9VZQEOTPiTnovxCS_uk8dBmW1VaU=' \
     * --form 'image=@"/path/to/file"'
     * 正常示例
     * {
     * "extra": {
     * "description": "",
     * "sub_error_code": 0,
     * "sub_description": "",
     * "now": 1648797449,
     * "logid": "021648797449846fd1b1111000700600000000000000086dfb920",
     * "error_code": 0
     * },
     * "data": {
     * "error_code": 0,
     * "description": ""
     * },
     * "image_id":"@baihq==",
     * "width":111
     * "height":111
     * "md5":""
     * }
     * 异常示例
     * {
     * "extra": {
     * "sub_description": "",
     * "sub_error_code": 0,
     * "description": "参数不合法",
     * "error_code": 2100005,
     * "logid": "202203271807199888808121990491456D",
     * "now": 1648375639
     * },
     * "data": {
     * "error_code": 2100005,
     * "description": "参数不合法"
     * }
     * }
     */
    @Override
    public DyClientUploadResponse clientUpload(DyClientUploadRequest dyClientUploadRequest) {
        return null;
    }
}
