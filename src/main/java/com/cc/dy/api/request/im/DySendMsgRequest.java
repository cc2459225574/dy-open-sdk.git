package com.cc.dy.api.request.im;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 主动发送私信
 * https://open.douyin.com/im/authorize/send/msg
 */
@Data
@Accessors(chain = true)
public class DySendMsgRequest {
    /**
     * 描述	通过/oauth/access_token/获取，主动发消息用户唯一标志，与用户主动授权( im_authorize )webhook中from_user_id相等
     * 是否必填	true
     * 示例值	0da22181-d833-447f-995f-1beefea5bef3
     */
    @JSONField(name = "open_id")
    @JsonAlias(value = "open_id")
    @JsonProperty(value = "open_id")
    @SerializedName(value = "open_id")
    private String openId;

    /**
     * 描述	消息的接收方openid，来自用户主动授权( im_authorize )webhook中to_user_id字段
     * 是否必填	true
     * 示例值	0da22181-d833-447f-995f-1beefea5bef3
     */
    @JSONField(name = "to_user_id")
    @JsonAlias(value = "to_user_id")
    @JsonProperty(value = "to_user_id")
    @SerializedName(value = "to_user_id")
    private String toUserId;

    /**
     * 描述	true
     * 是否必填	struct
     * 示例值	消息体，见下方 content 构造,文本 ,图片 ,视频 ontent,群邀请 ,小程序引导卡片
     */
    @JSONField(name = "content")
    @JsonAlias(value = "content")
    @JsonProperty(value = "content")
    @SerializedName(value = "content")
    private ContentStruct content;


    @Data
    public static class ContentStruct {

        @JSONField(name = "msg_type")
        @JsonAlias(value = "msg_type")
        @JsonProperty(value = "msg_type")
        @SerializedName(value = "msg_type")
        private Integer msgType;

        @JSONField(name = "image")
        @JsonAlias(value = "image")
        @JsonProperty(value = "image")
        @SerializedName(value = "image")
        private Image image;

        @JSONField(name = "video")
        @JsonAlias(value = "video")
        @JsonProperty(value = "video")
        @SerializedName(value = "video")
        private Video video;

        @JSONField(name = "applet_card")
        @JsonAlias(value = "applet_card")
        @JsonProperty(value = "applet_card")
        @SerializedName(value = "applet_card")
        private AppletCard appletCard;
    }

    @Data
    public static class Image {
        @JSONField(name = "media_id")
        @JsonAlias(value = "media_id")
        @JsonProperty(value = "media_id")
        @SerializedName(value = "media_id")
        private Integer mediaId;
    }

    @Data
    public static class Video {
        @JSONField(name = "item_id")
        @JsonAlias(value = "item_id")
        @JsonProperty(value = "item_id")
        @SerializedName(value = "item_id")
        private Integer itemId;
    }

    @Data
    public static class AppletCard {

        @JSONField(name = "card_template_id")
        @JsonAlias(value = "card_template_id")
        @JsonProperty(value = "card_template_id")
        @SerializedName(value = "card_template_id")
        private Integer cardTemplateId;


        @JSONField(name = "path")
        @JsonAlias(value = "path")
        @JsonProperty(value = "path")
        @SerializedName(value = "path")
        private Integer path;


        @JSONField(name = "query")
        @JsonAlias(value = "query")
        @JsonProperty(value = "query")
        @SerializedName(value = "query")
        private Integer query;


        @JSONField(name = "app_id")
        @JsonAlias(value = "app_id")
        @JsonProperty(value = "app_id")
        @SerializedName(value = "app_id")
        private Integer appId;
    }
}
