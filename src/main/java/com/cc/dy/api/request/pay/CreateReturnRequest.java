package com.cc.dy.api.request.pay;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * CreateReturnRequest
 *
 * <p>
 *
 * @author jiangcheng
 * @version 1.0
 * @since 2023/9/4 17:44
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CreateReturnRequest extends AbstractPayRequest {

    /**
     * 第三方平台服务商 id，非服务商模式留空
     */
    @JSONField(name = "thirdparty_id")
    @JsonAlias("thirdparty_id")
    @JsonProperty("thirdparty_id")
    @SerializedName("thirdparty_id")
    private String thirdPartyId;

    /**
     * 开发者侧的结算号,与out_settle_no二选一填写
     */
    @JSONField(name = "settle_no")
    @JsonAlias("settle_no")
    @JsonProperty("settle_no")
    @SerializedName("settle_no")
    private Long settleNo;

    /**
     * 商户侧的结算号，与settle_no二选一填写
     */
    @JSONField(name = "out_settle_no")
    @JsonAlias("out_settle_no")
    @JsonProperty("out_settle_no")
    @SerializedName("out_settle_no")
    private String outSettleNo;

    /**
     * 开发者回退单号，相同回退单号小程序平台会进行幂等处理。 只能使用数字、大小写字母_-*。
     */
    @JSONField(name = "out_return_no")
    @JsonAlias("out_return_no")
    @JsonProperty("out_return_no")
    @SerializedName("out_return_no")
    private String outReturnNo;

    /**
     * 回退描述，长度限制 100 个字符
     */
    @JSONField(name = "return_desc")
    @JsonAlias("return_desc")
    @JsonProperty("return_desc")
    @SerializedName("return_desc")
    private Long returnDesc;

    /**
     * 回退商户号，即原分账方商户号
     */
    @JSONField(name = "merchant_uid")
    @JsonAlias("merchant_uid")
    @JsonProperty("merchant_uid")
    @SerializedName("merchant_uid")
    private String merchantUid;

    /**
     * 回退金额，单位分，取值范围：[1,10000000000]
     */
    @JSONField(name = "return_amount")
    @JsonAlias("return_amount")
    @JsonProperty("return_amount")
    @SerializedName("return_amount")
    private Long returnAmount;

    /**
     * 开发者自定义字段，回调原样回传
     */
    @JSONField(name = "cp_extra")
    @JsonAlias("cp_extra")
    @JsonProperty("cp_extra")
    @SerializedName("cp_extra")
    private String cpExtra;
}
