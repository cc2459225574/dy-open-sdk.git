package com.cc.dy.api.request.auth;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 获取用户授权调用凭证
 * https://open.douyin.com/oauth/access_token
 */
@Data
@Accessors(chain = true)
public class DyAccessTokenRequest {
    /**
     * 描述	应用唯一标识对应的密钥
     * 是否必填	是
     * 示例值	7802f4e6f243e659d51135445fe******
     */
    @JSONField(name = "client_secret")
    @JsonAlias(value = "client_secret")
    @JsonProperty(value = "client_secret")
    @SerializedName(value = "client_secret")
    private String clientSecret;

    /**
     * 描述	用户授权码
     * 是否必填	是
     * 示例值	ffab5ec26cd958fditn2GNr8Wx5m0i******
     */
    @JSONField(name = "code")
    @JsonAlias(value = "code")
    @JsonProperty(value = "code")
    @SerializedName(value = "code")
    private String code;

    /**
     * 描述	固定值"authorization_code"
     * 是否必填	是
     * 示例值	authorization_code
     */
    @JSONField(name = "grant_type")
    @JsonAlias(value = "grant_type")
    @JsonProperty(value = "grant_type")
    @SerializedName(value = "grant_type")
    private String grantType;

    /**
     * 描述	应用唯一标识
     * 是否必填	是
     * 示例值	tt10abc****
     */
    @JSONField(name = "client_key")
    @JsonAlias(value = "client_key")
    @JsonProperty(value = "client_key")
    @SerializedName(value = "client_key")
    private String clientKey;

}
