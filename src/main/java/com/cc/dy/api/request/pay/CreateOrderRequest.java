package com.cc.dy.api.request.pay;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author chenChao 2023-08-28 10:51
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CreateOrderRequest extends AbstractPayRequest {

    /**
     * 开发者侧的订单号。 只能是数字、大小写字母_-*且在同一个app_id下唯一
     */
    @JSONField(name = "out_order_no")
    @JsonAlias("out_order_no")
    @JsonProperty("out_order_no")
    @SerializedName("out_order_no")
    private String outOrderNo;

    /**
     * 支付价格。 单位为[分]
     */
    @JSONField(name = "total_amount")
    @JsonAlias("total_amount")
    @JsonProperty("total_amount")
    @SerializedName("total_amount")
    private Long totalAmount;

    /**
     * 商品描述。 长度限制不超过 128 字节且不超过 42 字符
     */
    @JSONField(name = "subject")
    @JsonAlias("subject")
    @JsonProperty("subject")
    @SerializedName("subject")
    private String subject;

    /**
     * 商品详情 长度限制不超过 128 字节且不超过 42 字符
     */
    @JSONField(name = "body")
    @JsonAlias("body")
    @JsonProperty("body")
    @SerializedName("body")
    private String body;

    /**
     * 订单过期时间(秒)。最小5分钟，最大2天，小于5分钟会被置为5分钟，大于2天会被置为2天
     */
    @JSONField(name = "valid_time")
    @JsonAlias("valid_time")
    @JsonProperty("valid_time")
    @SerializedName("valid_time")
    private Long validTime;

    /**
     * 开发者自定义字段，回调原样回传。 超过最大长度会被截断
     */
    @JSONField(name = "cp_extra")
    @JsonAlias("cp_extra")
    @JsonProperty("cp_extra")
    @SerializedName("cp_extra")
    private String cpExtra;

    /**
     * 商户自定义回调地址，必须以 https 开头，支持 443 端口。 指定时，支付成功后抖音会请求该地址通知开发者
     */
    @JSONField(name = "notify_url")
    @JsonAlias("notify_url")
    @JsonProperty("notify_url")
    @SerializedName("notify_url")
    private String notifyUrl;

    /**
     * 第三方平台服务商 id，非服务商模式留空
     */
    @JSONField(name = "thirdparty_id")
    @JsonAlias("thirdparty_id")
    @JsonProperty("thirdparty_id")
    @SerializedName("thirdparty_id")
    private String thirdPartyId;

    /**
     * 可用此字段指定本单使用的收款商户号（目前为灰度功能，需要联系平台运营添加白名单，白名单添加1小时后生效；未在白名单的小程序，该字段不生效）
     */
    @JSONField(name = "store_uid")
    @JsonAlias("store_uid")
    @JsonProperty("store_uid")
    @SerializedName("store_uid")
    private String storeUid;

    /**
     * 是否屏蔽支付完成后推送用户抖音消息，1-屏蔽 0-非屏蔽，默认为0。 特别注意： 若接入POI, 请传1。因为POI订单体系会发消息，所以不用再接收一次担保支付推送消息，
     */
    @JSONField(name = "disable_msg")
    @JsonAlias("disable_msg")
    @JsonProperty("disable_msg")
    @SerializedName("disable_msg")
    private Long disableMsg;

    /**
     * 支付完成后推送给用户的抖音消息跳转页面，开发者需要传入在app.json中定义的链接，如果不传则跳转首页。
     */
    @JSONField(name = "msg_page")
    @JsonAlias("msg_page")
    @JsonProperty("msg_page")
    @SerializedName("msg_page")
    private String msgPage;

    /**
     * 订单拓展信息
     */
    @JSONField(name = "expand_order_info")
    @JsonAlias("expand_order_info")
    @JsonProperty("expand_order_info")
    @SerializedName("expand_order_info")
    private ExpandOrderInfo expandOrderInfo;

    /**
     * 屏蔽指定支付方式，屏蔽多个支付方式，请使用逗号","分割，枚举值：
     * 屏蔽微信支付：LIMIT_WX
     * 屏蔽支付宝支付：LIMIT_ALI
     * 屏蔽抖音支付：LIMIT_DYZF
     * <p>
     * 特殊说明：若之前开通了白名单，平台会保留之前屏蔽逻辑；若传入该参数，会优先以传入的为准，白名单则无效
     */
    @JSONField(name = "limit_pay_way")
    @JsonAlias("limit_pay_way")
    @JsonProperty("limit_pay_way")
    @SerializedName("limit_pay_way")
    private String limitPayWay;

    @Data
    public static class ExpandOrderInfo implements Serializable {
        /**
         * 配送费原价，单位为[分]，仅外卖小程序需要传对应信息
         */
        @JSONField(name = "original_delivery_fee")
        @JsonAlias("original_delivery_fee")
        @JsonProperty("original_delivery_fee")
        @SerializedName("original_delivery_fee")
        private Long originalDeliveryFee;

        /**
         * 实付配送费，单位为[分]，仅外卖小程序需要传对应信息
         */
        @JSONField(name = "actual_delivery_fee")
        @JsonAlias("actual_delivery_fee")
        @JsonProperty("actual_delivery_fee")
        @SerializedName("actual_delivery_fee")
        private Long actualDeliveryFee;
    }
}
