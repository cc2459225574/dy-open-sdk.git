package com.cc.dy.api.request.pay;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author chenChao 2023-08-28 17:07
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CreateRefundRequest extends AbstractPayRequest {

    /**
     * 商户分配支付单号，标识进行退款的订单
     */
    @JSONField(name = "out_order_no")
    @JsonAlias("out_order_no")
    @JsonProperty("out_order_no")
    @SerializedName("out_order_no")
    private String outOrderNo;
    /**
     * 商户分配退款号，保证在商户中唯一
     */
    @JSONField(name = "out_refund_no")
    @JsonAlias("out_refund_no")
    @JsonProperty("out_refund_no")
    @SerializedName("out_refund_no")
    private String outRefundNo;
    /**
     * 退款原因
     */
    @JSONField(name = "reason")
    @JsonAlias("reason")
    @JsonProperty("reason")
    @SerializedName("reason")
    private String reason;
    /**
     * 退款金额，单位分
     */
    @JSONField(name = "refund_amount")
    @JsonAlias("refund_amount")
    @JsonProperty("refund_amount")
    @SerializedName("refund_amount")
    private Long refundAmount;
    /**
     * 开发者自定义字段，回调原样回传
     */
    @JSONField(name = "cp_extra")
    @JsonAlias("cp_extra")
    @JsonProperty("cp_extra")
    @SerializedName("cp_extra")
    private String cpExtra;
    /**
     * 商户自定义回调地址，必须以 https 开头，支持 443 端口
     */
    @JSONField(name = "notify_url")
    @JsonAlias("notify_url")
    @JsonProperty("notify_url")
    @SerializedName("notify_url")
    private String notifyUrl;
    /**
     * 第三方平台服务商id，服务商模式接入必传，非服务商模式留空
     */
    @JSONField(name = "thirdparty_id")
    @JsonAlias("thirdparty_id")
    @JsonProperty("thirdparty_id")
    @SerializedName("thirdparty_id")
    private String thirdPartyId;
    /**
     * 是否屏蔽担保支付的推送消息枚举值：
     * 1：屏蔽
     * 0：非屏蔽， 若接入POI，POI订单体系会发相关消息，所以不用再接收一次担保支付相关的消息，请传1
     */
    @JSONField(name = "disable_msg")
    @JsonAlias("disable_msg")
    @JsonProperty("disable_msg")
    @SerializedName("disable_msg")
    private Long disableMsg;
    /**
     * 退款完成后推送给用户的抖音消息跳转页面，开发者需要传入在app.json中定义的链接，如果不传则跳转首页。
     */
    @JSONField(name = "msg_page")
    @JsonAlias("msg_page")
    @JsonProperty("msg_page")
    @SerializedName("msg_page")
    private String msgPage;
}
