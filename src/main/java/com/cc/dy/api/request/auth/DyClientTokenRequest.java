package com.cc.dy.api.request.auth;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 获取应用授权调用凭证
 * POST https://open.douyin.com/oauth/client_token
 */
@Data
@Accessors(chain = true)
public class DyClientTokenRequest {
    /**
     * 描述	应用唯一标识，对应小程序id
     * 是否必填	是
     * 示例值	ttxxxxxx
     */
    @JSONField(name = "client_key")
    @JsonAlias(value = "client_key")
    @JsonProperty(value = "client_key")
    @SerializedName(value = "client_key")
    private String clientKey;

    /**
     * 描述	应用唯一标识对应的密钥，对应小程序的app secret，可以在开发者后获取
     * 是否必填	是
     * 示例值	7802f4e6f243e659d51135445fe******
     */
    @JSONField(name = "client_secret")
    @JsonAlias(value = "client_secret")
    @JsonProperty(value = "client_secret")
    @SerializedName(value = "client_secret")
    private String clientSecret;

    /**
     * 描述	固定值“client_credential”
     * 是否必填	是
     * 示例值	client_credential
     */
    @JSONField(name = "grant_type")
    @JsonAlias(value = "grant_type")
    @JsonProperty(value = "grant_type")
    @SerializedName(value = "grant_type")
    private String grantType = "client_credential";

}
