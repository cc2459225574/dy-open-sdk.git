package com.cc.dy.api.request.link;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * createQRCodeV2
 * https://open.douyin.com/api/apps/v1/qrcode/create
 */
@Data
@Accessors(chain = true)
public class DyCreateQrcodeRequestV2 {
    /**
     * 描述	    是打开二维码的字节系 app 名称，默认为今日头条，取值如下表所示
     * 今日头条: toutiao,今日头条极速版: toutiao_lite,抖音: douyin,抖音极速版: douyin_lite,皮皮虾: pipixia,火山小视频: huoshan,西瓜视频: xigua
     * 是否必填	    false
     * 示例值	    toutiao
     */
    @JSONField(name = "app_name")
    @JsonAlias(value = "app_name")
    @JsonProperty(value = "app_name")
    @SerializedName(value = "app_name")
    private String appName;

    /**
     * 描述	    小程序id
     * 是否必填	    false
     * 示例值	    tt********
     */
    @JSONField(name = "    appid")
    @JsonAlias(value = "    appid")
    @JsonProperty(value = "    appid")
    @SerializedName(value = "    appid")
    private String appid;

    /**
     * 描述	    二维码背景颜色，默认为白色
     * 是否必填	    false
     * 示例值
     */
    @JSONField(name = "    background")
    @JsonAlias(value = "    background")
    @JsonProperty(value = "    background")
    @SerializedName(value = "    background")
    private Background background;

    /**
     * 描述	    二维码线条颜色，默认为黑色
     * 是否必填	    false
     * 示例值
     */
    @JSONField(name = "    line_color")
    @JsonAlias(value = "    line_color")
    @JsonProperty(value = "    line_color")
    @SerializedName(value = "    line_color")
    private LineColor lineColor;

    /**
     * 描述	    小程序/小游戏启动参数，小程序则格式为 encode({path}?{query})，小游戏则格式为 JSON 字符串，默认为空
     * 是否必填	    false
     * 示例值
     */
    @JSONField(name = "    path")
    @JsonAlias(value = "    path")
    @JsonProperty(value = "    path")
    @SerializedName(value = "    path")
    private String path;

    /**
     * 描述	    是否展示小程序/小游戏 icon，默认不展示
     * 是否必填	    false
     * 示例值	    FALSE
     */
    @JSONField(name = "    set_icon")
    @JsonAlias(value = "    set_icon")
    @JsonProperty(value = "    set_icon")
    @SerializedName(value = "    set_icon")
    private Boolean setIcon;

    /**
     * 描述	    二维码宽度，单位 px，最小 280px，最大 1280px，默认为 430px
     * 是否必填	    false
     * 示例值	    430
     */
    @JSONField(name = "    width")
    @JsonAlias(value = "    width")
    @JsonProperty(value = "    width")
    @SerializedName(value = "    width")
    private Integer width;

    @Data
    public static class Background {
        /**
         * 描述
         * 是否必填	false
         * 示例值
         */
        @JSONField(name = "b")
        @JsonAlias(value = "b")
        @JsonProperty(value = "b")
        @SerializedName(value = "b")
        private Integer b;

        /**
         * 描述
         * 是否必填	false
         * 示例值
         */
        @JSONField(name = "g")
        @JsonAlias(value = "g")
        @JsonProperty(value = "g")
        @SerializedName(value = "g")
        private Integer g;

        /**
         * 描述
         * 是否必填	false
         * 示例值
         */
        @JSONField(name = "r")
        @JsonAlias(value = "r")
        @JsonProperty(value = "r")
        @SerializedName(value = "r")
        private Integer r;
    }

    @Data
    public static class LineColor {
        /**
         * 描述
         * 是否必填	false
         * 示例值
         */
        @JSONField(name = "b")
        @JsonAlias(value = "b")
        @JsonProperty(value = "b")
        @SerializedName(value = "b")
        private Integer b;

        /**
         * 描述
         * 是否必填	false
         * 示例值
         */
        @JSONField(name = "g")
        @JsonAlias(value = "g")
        @JsonProperty(value = "g")
        @SerializedName(value = "g")
        private Integer g;

        /**
         * 描述
         * 是否必填	false
         * 示例值
         */
        @JSONField(name = "r")
        @JsonAlias(value = "r")
        @JsonProperty(value = "r")
        @SerializedName(value = "r")
        private Integer r;
    }
}
