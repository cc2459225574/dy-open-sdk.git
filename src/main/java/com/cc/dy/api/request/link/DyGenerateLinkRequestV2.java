package com.cc.dy.api.request.link;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * GenerateV2
 * https://open.douyin.com/api/apps/v1/url_link/generate
 */
@Data
@Accessors(chain = true)
public class DyGenerateLinkRequestV2 {

    /**
     * 描述	宿主名称，可选 douyin，douyinlite
     * 是否必填	是
     * 示例值	douyin
     */
    @JSONField(name = "app_name")
    @JsonAlias(value = "app_name")
    @JsonProperty(value = "app_name")
    @SerializedName(value = "app_name")
    private String appName;


    /**
     * 描述	小程序 ID
     * 是否必填	是
     * 示例值	tt**************
     */
    @JSONField(name = "app_id")
    @JsonAlias(value = "app_id")
    @JsonProperty(value = "app_id")
    @SerializedName(value = "app_id")
    private String appId;

    /**
     * 描述	通过 URL Link 进入的小程序页面路径，必须是已经发布的小程序存在的页面，不可携带 query。path 为空时会跳转小程序主页。
     * 是否必填	否
     * 示例值	pages/index
     */
    @JSONField(name = "path")
    @JsonAlias(value = "path")
    @JsonProperty(value = "path")
    @SerializedName(value = "path")
    private String path;

    /**
     * 描述	通过 URL Link 进入小程序时的 query（JSON 形式），若无请填 {}。最大 1024 个字符，只支持数字，大小写英文以及部分特殊字符：`{}!#$&'()*+,/:;=?@-._~%``。
     * 是否必填	否
     * 示例值	{"id":"123***789"}
     */
    @JSONField(name = "query")
    @JsonAlias(value = "query")
    @JsonProperty(value = "query")
    @SerializedName(value = "query")
    private String query;

    /**
     * 描述	到期失效的 URL Link 的失效时间。为 Unix 时间戳，实际失效时间为距离当前时间小时数，向上取整。最长间隔天数为 180 天。
     * 是否必填	是
     * 示例值	1665158399
     */
    @JSONField(name = "expire_time")
    @JsonAlias(value = "expire_time")
    @JsonProperty(value = "expire_time")
    @SerializedName(value = "expire_time")
    private Long expireTime;

}
