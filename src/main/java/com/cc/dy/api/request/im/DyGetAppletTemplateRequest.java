package com.cc.dy.api.request.im;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.experimental.Accessors;
/**
*查询小程序引导卡片模板
*https://open.douyin.com/api/douyin/v1/im/get_applet_template*/
@Data
@Accessors(chain = true)
public class DyGetAppletTemplateRequest {
    /**
     *描述	卡片模板ID
     *是否必填	false
     *示例值	@8hxdhauTCMppanGnM4ltGM780mDqPP+KPpR0qQOmLVAXb/T060zdRmYqig357zEBq6CZRp4NVe6qLIJW/V/x1w==
     */
    @JSONField(name = "card_template_id")
    @JsonAlias(value = "card_template_id")
    @JsonProperty(value = "card_template_id")
    @SerializedName(value = "card_template_id")
    private String cardTemplateId;

    /**
     *描述	每页数量，不传 card_template_id 时，该值必传，最大值为 50
     *是否必填	false
     *示例值	20
     */
    @JSONField(name = "count")
    @JsonAlias(value = "count")
    @JsonProperty(value = "count")
    @SerializedName(value = "count")
    private Integer count;

    /**
     *描述	分页游标
     *是否必填	false
     *示例值	21
     */
    @JSONField(name = "cursor")
    @JsonAlias(value = "cursor")
    @JsonProperty(value = "cursor")
    @SerializedName(value = "cursor")
    private Integer cursor;

    /**
     *描述	卡片状态，缺省则返回所有状态的数据
     *是否必填	false
     *示例值	1
     */
    @JSONField(name = "status")
    @JsonAlias(value = "status")
    @JsonProperty(value = "status")
    @SerializedName(value = "status")
    private Integer status;
}
