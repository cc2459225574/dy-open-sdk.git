package com.cc.dy.api.request.link;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 生成SchemaV2
 * https://open.douyin.com/api/apps/v1/url/generate_schema
 */
@Data
@Accessors(chain = true)
public class DyGenerateSchemaRequestV2 {
    /**
     * 描述	小程序ID
     * 是否必填	是
     * 示例值	tt**************
     */
    @JSONField(name = "app_id")
    @JsonAlias(value = "app_id")
    @JsonProperty(value = "app_id")
    @SerializedName(value = "app_id")
    private String appId;

    /**
     * 描述	通过 schema 进入的小程序页面路径，必须是已经发布的小程序存在的页面，不可携带 query。path 为空时会跳转小程序主页。
     * 是否必填	否
     * 示例值	pages/index
     */
    @JSONField(name = "path")
    @JsonAlias(value = "path")
    @JsonProperty(value = "path")
    @SerializedName(value = "path")
    private String path;

    /**
     * 描述	通过 schema 进入小程序时的 query（json形式）。最大1024个字符，只支持数字，大小写英文以及部分特殊字符：`{}!#$&'()*+,/:;=?@-._~%``。
     * 是否必填	否
     * 示例值	{"id":"123***789"}
     */
    @JSONField(name = "query")
    @JsonAlias(value = "query")
    @JsonProperty(value = "query")
    @SerializedName(value = "query")
    private String query;

    /**
     * 描述	到期失效的 schema 的失效时间，为 Unix 时间戳，短期有效schema必传，最长间隔天数为180天。
     * 是否必填	否
     * 示例值	1685090014
     */
    @JSONField(name = "expire_time")
    @JsonAlias(value = "expire_time")
    @JsonProperty(value = "expire_time")
    @SerializedName(value = "expire_time")
    private Long expireTime;

    /**
     * 描述	生成的 scheme 码类型，短期有效:false，长期有效:true。默认值false。
     * 是否必填	是
     * 示例值	true
     */
    @JSONField(name = "no_expire")
    @JsonAlias(value = "no_expire")
    @JsonProperty(value = "no_expire")
    @SerializedName(value = "no_expire")
    private boolean noExpire;

}
