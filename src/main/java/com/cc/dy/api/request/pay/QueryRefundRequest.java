package com.cc.dy.api.request.pay;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * QueryRefundRequest
 *
 * <p>
 *
 * @author jiangcheng
 * @version 1.0
 * @since 2023/9/4 16:51
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class QueryRefundRequest extends AbstractPayRequest{
    @JSONField(name = "out_refund_no")
    @JsonAlias("out_refund_no")
    @JsonProperty("out_refund_no")
    @SerializedName("out_refund_no")
    private String outRefundNo;

    @JSONField(name = "thirdparty_id")
    @JsonAlias("thirdparty_id")
    @JsonProperty("thirdparty_id")
    @SerializedName("thirdparty_id")
    private String thirdPartyId;
}
