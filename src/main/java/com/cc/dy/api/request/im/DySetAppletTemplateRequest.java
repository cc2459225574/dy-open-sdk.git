package com.cc.dy.api.request.im;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 创建/更新小程序引导卡片模板
 * https://open.douyin.com/api/douyin/v1/im/set_applet_template
 */
@Data
@Accessors(chain = true)
public class DySetAppletTemplateRequest {
    /**
     * 描述	模板ID，更新模板时传入
     * 是否必填	false
     * 示例值	@8hxdhauTCMppanGnM4ltGM780mDqPP+KPpR0qQOmLVAXb/T060zdRmYqig357zEBq6CZRp4NVe6qLIJW/V/x1w==
     */
    @JSONField(name = "card_template_id")
    @JsonAlias(value = "card_template_id")
    @JsonProperty(value = "card_template_id")
    @SerializedName(value = "card_template_id")
    private String cardTemplateId;

    /**
     * 描述	卡片类型，创建模板时必传 1：小程序卡片
     * 是否必填	false
     * 示例值	1
     */
    @JSONField(name = "card_type")
    @JsonAlias(value = "card_type")
    @JsonProperty(value = "card_type")
    @SerializedName(value = "card_type")
    private Integer cardType;

    /**
     * 描述	卡片内容，创建模板时必传，最长 30 字
     * 是否必填	false
     * 示例值	点击加入
     */
    @JSONField(name = "content")
    @JsonAlias(value = "content")
    @JsonProperty(value = "content")
    @SerializedName(value = "content")
    private String content;

    /**
     * 描述	图片的 ID，调用图片上传接口获取。创建模板时必传,推荐比例16:9，尺寸240*135
     * 是否必填	false
     * 示例值	@8hxdhauTCMppanGnM4ltGM780mDqPP+KPpR0qQOmLVAXb/T060zdRmYqig357zEBq6CZRp4NVe6qLIJW/V/x1w==
     */
    @JSONField(name = "media_id")
    @JsonAlias(value = "media_id")
    @JsonProperty(value = "media_id")
    @SerializedName(value = "media_id")
    private String mediaId;

    /**
     * 描述	小程序 AppID
     * 是否必填	false
     * 示例值	tt5daf2b12c11da13910
     */
    @JSONField(name = "app_id")
    @JsonAlias(value = "app_id")
    @JsonProperty(value = "app_id")
    @SerializedName(value = "app_id")
    private String appId;

    /**
     * 描述	卡片标题,创建模板时必传，最长 30 字
     * 是否必填	false
     * 示例值  邀请你一起打荣耀巅峰匹配赛
     */
    @JSONField(name = "title")
    @JsonAlias(value = "title")
    @JsonProperty(value = "title")
    @SerializedName(value = "title")
    private String title;
}
