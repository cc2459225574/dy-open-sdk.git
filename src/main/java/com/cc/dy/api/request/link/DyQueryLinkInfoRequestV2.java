package com.cc.dy.api.request.link;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * QueryInfoV2
 * https://open.douyin,com/api/apps/v1/url_link/query_info
 */
@Data
@Accessors(chain = true)
public class DyQueryLinkInfoRequestV2 {
    /**
     * 描述	小程序 ID
     * 是否必填	否
     * 示例值	tt**************
     */
    @JSONField(name = "app_id")
    @JsonAlias(value = "app_id")
    @JsonProperty(value = "app_id")
    @SerializedName(value = "app_id")
    private String appId;

    /**
     * 是否必填	否
     */
    @JSONField(name = "url_link")
    @JsonAlias(value = "url_link")
    @JsonProperty(value = "url_link")
    @SerializedName(value = "url_link")
    private String urlLink;
}
