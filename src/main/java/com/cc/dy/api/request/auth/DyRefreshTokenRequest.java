package com.cc.dy.api.request.auth;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 刷新用户授权调用凭证
 * https://open.douyin.com/oauth/refresh_token/沙盒环境：https://open-sandbox.douyin.com/oauth/refresh_token
 */
@Data
@Accessors(chain = true)
public class DyRefreshTokenRequest {
    /**
     * 描述	应用唯一标识
     * 是否必填	是
     * 示例值	tt10abc****
     */
    @JSONField(name = "client_key")
    @JsonAlias(value = "client_key")
    @JsonProperty(value = "client_key")
    @SerializedName(value = "client_key")
    private String clientKey;

    /**
     * 描述	固定值“refresh_token”
     * 是否必填	是
     * 示例值	refresh_token
     */
    @JSONField(name = "grant_type")
    @JsonAlias(value = "grant_type")
    @JsonProperty(value = "grant_type")
    @SerializedName(value = "grant_type")
    private String grantType = "refresh_token";

    /**
     * 描述	填写通过 access_token 获取到的 refresh_token 参数
     * 是否必填	是
     * 示例值	rft.a736b70544519999a623d67******
     */
    @JSONField(name = "refresh_token")
    @JsonAlias(value = "refresh_token")
    @JsonProperty(value = "refresh_token")
    @SerializedName(value = "refresh_token")
    private String refreshToken;

}
