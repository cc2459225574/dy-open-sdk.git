package com.cc.dy.api.request.im;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.experimental.Accessors;
/**
*删除小程序引导卡片模板
*https://open.douyin.com/api/douyin/v1/im/del_applet_template*/
@Data
@Accessors(chain = true)
public class DyDelAppletTemplateRequest {
    /**
     *描述	模板ID，删除模板时传入
     *是否必填	false
     *示例值	@8hxdhauTCMppanGnM4ltGM780mDqPP+KPpR0qQOmLVAXb/T060zdRmYqig357zEBq6CZRp4NVe6qLIJW/V/x1w==
     */
    @JSONField(name = "card_template_id")
    @JsonAlias(value = "card_template_id")
    @JsonProperty(value = "card_template_id")
    @SerializedName(value = "card_template_id")
    private String cardTemplateId;


}
