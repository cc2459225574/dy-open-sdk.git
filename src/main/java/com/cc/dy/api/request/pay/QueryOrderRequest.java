package com.cc.dy.api.request.pay;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author chenChao 2023-08-28 16:38
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class QueryOrderRequest extends AbstractPayRequest {

    @JSONField(name = "out_order_no")
    @JsonAlias("out_order_no")
    @JsonProperty("out_order_no")
    @SerializedName("out_order_no")
    private String outOrderNo;

    @JSONField(name = "thirdparty_id")
    @JsonAlias("thirdparty_id")
    @JsonProperty("thirdparty_id")
    @SerializedName("thirdparty_id")
    private String thirdPartyId;
}
