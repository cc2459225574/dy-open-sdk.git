package com.cc.dy.api.request.auth;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.io.Serializable;

@Data
public class DyJsCode2sessionRequest implements Serializable {
    /**
     * 小程序 ID
     */
    @JSONField(name = "appid")
    @JsonAlias(value = "appid")
    @JsonProperty(value = "appid")
    @SerializedName(value = "appid")
    private String appid;

    /**
     * 小程序的 APP Secret，可以在开发者后台获取
     */
    @JSONField(name = "secret")
    @JsonAlias(value = "secret")
    @JsonProperty(value = "secret")
    @SerializedName(value = "secret")
    private String secret;

    /**
     * login 接口返回的登录凭证
     */
    @JSONField(name = "code")
    @JsonAlias(value = "code")
    @JsonProperty(value = "code")
    @SerializedName(value = "code")
    private String code;

    /**
     * login 接口返回的匿名登录凭证
     */
    @JSONField(name = "anonymous_code")
    @JsonAlias(value = "anonymous_code")
    @JsonProperty(value = "anonymous_code")
    @SerializedName(value = "anonymous_code")
    private String anonymousCode;
}
