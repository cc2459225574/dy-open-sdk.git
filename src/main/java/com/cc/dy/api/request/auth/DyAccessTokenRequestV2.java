package com.cc.dy.api.request.auth;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class DyAccessTokenRequestV2 implements Serializable {
    /**
     * 小程序 ID
     */
    @JSONField(name = "appid")
    @JsonAlias(value = "appid")
    @JsonProperty(value = "appid")
    @SerializedName(value = "appid")
    private String appid;

    /**
     * 小程序的 APP Secret，可以在开发者后台获取
     */
    @JSONField(name = "secret")
    @JsonAlias(value = "secret")
    @JsonProperty(value = "secret")
    @SerializedName(value = "secret")
    private String secret;

    /**
     * 获取 access_token 时值为 client_credential
     */
    @JSONField(name = "grant_type")
    @JsonAlias(value = "grant_type")
    @JsonProperty(value = "grant_type")
    @SerializedName(value = "grant_type")
    private String grantType = "client_credential";
}
