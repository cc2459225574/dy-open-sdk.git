package com.cc.dy.api.request.link;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 查询SchemaV2
 * https://open.douyin.com/api/apps/v1/url/query_schema
 */
@Data
@Accessors(chain = true)
public class DyQuerySchemaRequestV2 {
    /**
     * 描述	生成的schema
     * 是否必填	是
     * 示例值	sslocal://miniapp?ticket=v1_********
     */
    @JSONField(name = "schema")
    @JsonAlias(value = "schema")
    @JsonProperty(value = "schema")
    @SerializedName(value = "schema")
    private String schema;

    /**
     * 描述	小程序ID
     * 是否必填	是
     * 示例值	tt*******
     */
    @JSONField(name = "app_id")
    @JsonAlias(value = "app_id")
    @JsonProperty(value = "app_id")
    @SerializedName(value = "app_id")
    private String appId;

}
