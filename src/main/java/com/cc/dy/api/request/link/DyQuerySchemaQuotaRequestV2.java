package com.cc.dy.api.request.link;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 查询Schema配额V2
 * https://open.douyin.com/api/apps/v1/url/query_schema_quota
 */
@Data
@Accessors(chain = true)
public class DyQuerySchemaQuotaRequestV2 {
    /**
     * 描述	小程序ID
     * 是否必填	是
     * 示例值	tt**************
     */
    @JSONField(name = "app_id")
    @JsonAlias(value = "app_id")
    @JsonProperty(value = "app_id")
    @SerializedName(value = "app_id")
    private String appId;

}
