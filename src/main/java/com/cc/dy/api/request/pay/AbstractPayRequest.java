package com.cc.dy.api.request.pay;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.io.Serializable;

/**
 * @author chenChao 2023-08-28 10:52
 */
@Data
public abstract class AbstractPayRequest implements Serializable {

    /**
     * 小程序 ID
     */
    @JSONField(name = "app_id")
    @JsonAlias("app_id")
    @JsonProperty("app_id")
    @SerializedName("app_id")
    private String appId;

    /**
     * 签名
     */
    @JSONField(name = "sign")
    @JsonAlias("sign")
    @JsonProperty("sign")
    @SerializedName("sign")
    private String sign;
}
