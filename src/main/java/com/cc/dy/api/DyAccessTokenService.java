package com.cc.dy.api;

import com.cc.dy.api.response.auth.DyAccessTokenResponseV2;

import java.util.Map;

public interface DyAccessTokenService {

    /**
     * access_token 是小程序的全局唯一调用凭据，开发者调用小程序支付时需要使用 access_token。access_token 的有效期为 2 个小时，需要定时刷新 access_token，重复获取会导致之前一次获取的 access_token 的有效期缩短为 5 分钟。
     *
     * @return {@link DyAccessTokenResponseV2}
     */
    String getAccessToken();

    /**
     * 刷新token.
     *
     * @return token
     */
    String refreshToken();

    /**
     * 生产token请求头信息
     *
     * @return 请求头信息
     */
    Map<String, String> getAccessTokenMap();
}
