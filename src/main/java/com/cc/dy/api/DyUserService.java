package com.cc.dy.api;

import com.cc.dy.api.response.user.DyMobileResponse;
import com.cc.dy.api.response.user.DyUserInfoResponse;

/**
 *
 */
public interface DyUserService {

    /**
     * 获取手机号码
     *
     * @param sessionKey    开发者通过code2session接口得到的session_key。
     * @param encryptedData 包括敏感数据在内的完整用户信息的加密数据
     * @param iv            加密算法的初始向量
     * @return 手机号码
     */
    DyMobileResponse getMobile(String sessionKey, String encryptedData, String iv);

    /**
     * 获取已登录用户的基本信息或特殊信息，首次使用的用户会弹出授权提示窗，若用户同意，则会返回用户的真实数据。
     *
     * @param sessionKey    开发者通过code2session接口得到的session_key。
     * @param encryptedData 包括敏感数据在内的完整用户信息的加密数据
     * @param iv            加密算法的初始向量
     * @return 用户的基本信息
     */
    DyUserInfoResponse getUserInfo(String sessionKey, String encryptedData, String iv);
}
