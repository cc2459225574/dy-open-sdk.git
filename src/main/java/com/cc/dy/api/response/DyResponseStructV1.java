package com.cc.dy.api.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author chenChao 2023-09-15 15:07
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DyResponseStructV1 extends DyResponse implements Serializable {

}
