package com.cc.dy.api.response.im;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 查询小程序引导卡片模板
 * https://open.douyin.com/api/douyin/v1/im/get_applet_template
 */
@Data
@Accessors(chain = true)
public class DyGetAppletTemplateResponse {
    /**
     * 描述
     * 是否必填
     * 示例值	12
     */
    @JSONField(name = "cursor")
    @JsonAlias(value = "cursor")
    @JsonProperty(value = "cursor")
    @SerializedName(value = "cursor")
    private Integer cursor;

    /**
     * 描述
     * 是否必填
     * 示例值	false
     */
    @JSONField(name = "has_more")
    @JsonAlias(value = "has_more")
    @JsonProperty(value = "has_more")
    @SerializedName(value = "has_more")
    private Boolean hasMore;

    /**
     * 描述	list<struct>
     * 是否必填
     * 示例值
     */
    @JSONField(name = "list")
    @JsonAlias(value = "list")
    @JsonProperty(value = "list")
    @SerializedName(value = "list")
    private List<Card> list;

    @Data
    public static class Card {
        /**
         * 描述	图片类型消息的图片资源ID
         * 是否必填
         * 示例值	@8hxdhauTCMppanGnM4ltGM780mDqPP+KPpR0qQOmLVAXb/T060zdRmYqig357zEBq6CZRp4NVe6qLIJW/V/x1w==
         */
        @JSONField(name = "card_template_id")
        @JsonAlias(value = "card_template_id")
        @JsonProperty(value = "card_template_id")
        @SerializedName(value = "card_template_id")
        private String cardTemplateId;

        /**
         * 描述	卡片类型
         * 是否必填
         * 示例值	1
         */
        @JSONField(name = "card_type")
        @JsonAlias(value = "card_type")
        @JsonProperty(value = "card_type")
        @SerializedName(value = "card_type")
        private Integer cardType;

        /**
         * 描述	卡片内容
         * 是否必填
         * 示例值	邀请你一起游戏
         */
        @JSONField(name = "content")
        @JsonAlias(value = "content")
        @JsonProperty(value = "content")
        @SerializedName(value = "content")
        private String content;

        /**
         * 描述	创建时间（秒级时间戳）
         * 是否必填
         * 示例值	1571075129
         */
        @JSONField(name = "create_time")
        @JsonAlias(value = "create_time")
        @JsonProperty(value = "create_time")
        @SerializedName(value = "create_time")
        private Integer createTime;

        /**
         * 描述	小程序应用图标 url
         * 是否必填
         * 示例值	https://p3-developer-sign.bytemaimg.com/tos-cn-i-ke512zj2cu/a37b56ae3da84c2b98e4ad3e1ac7a1320~tplv-ke512zj2cu-144x144.jpeg?x-expires=2628437565&x-signature=kRfqYlvF17z%2FYdspB5lHAEQBzQM%3D
         */
        @JSONField(name = "app_icon_url")
        @JsonAlias(value = "app_icon_url")
        @JsonProperty(value = "app_icon_url")
        @SerializedName(value = "app_icon_url")
        private String appIconUrl;

        /**
         * 描述	小程序 AppID
         * 是否必填
         * 示例值	tt5a3daab12c281517910
         */
        @JSONField(name = "app_id")
        @JsonAlias(value = "app_id")
        @JsonProperty(value = "app_id")
        @SerializedName(value = "app_id")
        private String appId;

        /**
         * 描述	小程序应用名称
         * 是否必填
         * 示例值	小程序应用
         */
        @JSONField(name = "name")
        @JsonAlias(value = "name")
        @JsonProperty(value = "name")
        @SerializedName(value = "name")
        private String name;

        /**
         * 描述	卡片状态,0：待审核,1：审核通过,2：审核未通过
         * 是否必填
         * 示例值	1
         */
        @JSONField(name = "status")
        @JsonAlias(value = "status")
        @JsonProperty(value = "status")
        @SerializedName(value = "status")
        private Integer status;

        /**
         * 描述	卡片标题
         * 是否必填
         * 示例值	卡片 title
         */
        @JSONField(name = "title")
        @JsonAlias(value = "title")
        @JsonProperty(value = "title")
        @SerializedName(value = "title")
        private String title;

        /**
         * 描述	更新时间（秒级时间戳）
         * 是否必填
         * 示例值	1571075129
         */
        @JSONField(name = "update_time")
        @JsonAlias(value = "update_time")
        @JsonProperty(value = "update_time")
        @SerializedName(value = "update_time")
        private Integer updateTime;

        /**
         * 描述	审核拒绝原因（若卡片状态为审核为未通过）
         * 是否必填
         * 示例值	详细拒绝原因
         */
        @JSONField(name = "reject_reasons")
        @JsonAlias(value = "reject_reasons")
        @JsonProperty(value = "reject_reasons")
        @SerializedName(value = "reject_reasons")
        private String rejectReasons;
    }
}
