package com.cc.dy.api.response;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author chenChao 2023-09-15 15:07
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DyResponseStructV4 extends DyResponse implements Serializable {
    /**
     * 描述	错误码
     * 是否必填	是
     * 示例值	0
     */
    @JSONField(name = "err_no")
    @JsonAlias(value = "err_no")
    @JsonProperty(value = "err_no")
    @SerializedName(value = "err_no")
    private Long errNo;

    /**
     * 描述	错误信息
     * 是否必填	是
     * 示例值	""
     */
    @JSONField(name = "err_msg")
    @JsonAlias(value = "err_msg")
    @JsonProperty(value = "err_msg")
    @SerializedName(value = "err_msg")
    private String errMsg;

    /**
     * 描述	字节内部log_id，用于问题快速定位
     * 是否必填	是
     * 示例值	"2027************"D3r1"
     */
    @JSONField(name = "log_id")
    @JsonAlias(value = "log_id")
    @JsonProperty(value = "log_id")
    @SerializedName(value = "log_id")
    private String logId;
}
