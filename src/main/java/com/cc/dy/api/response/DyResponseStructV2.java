package com.cc.dy.api.response;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author chenChao 2023-09-15 15:07
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DyResponseStructV2 extends DyResponse implements Serializable {

    /**
     * 请求响应
     */
    @JSONField(name = "data")
    @JsonAlias(value = "data")
    @JsonProperty(value = "data")
    @SerializedName(value = "data")
    private String message;

    /**
     * 描述	错误码描述
     * 示例值	""
     */
    @JSONField(name = "description")
    @JsonAlias(value = "description")
    @JsonProperty(value = "description")
    @SerializedName(value = "description")
    private String description;

    /**
     * 描述	错误码
     * 示例值	0
     */
    @JSONField(name = "error_code")
    @JsonAlias(value = "error_code")
    @JsonProperty(value = "error_code")
    @SerializedName(value = "error_code")
    private Long errorCode;


    @JSONField(name = "logid")
    @JsonAlias(value = "logid")
    @JsonProperty(value = "logid")
    @SerializedName(value = "logid")
    private String logId;


    @JSONField(name = "now")
    @JsonAlias(value = "now")
    @JsonProperty(value = "now")
    @SerializedName(value = "now")
    private String now;
}
