package com.cc.dy.api.response.pay;

import com.alibaba.fastjson.annotation.JSONField;
import com.cc.dy.api.response.DyResponseStructV3;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author chenChao 2023-08-28 10:55
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CreateOrderResponse extends DyResponseStructV3 {

    @JSONField(name = "data")
    @JsonAlias("data")
    @JsonProperty("data")
    @SerializedName("data")
    private Order data;

    @Data
    public static class Order implements Serializable {
        /**
         * 抖音侧唯一订单号
         */
        @JSONField(name = "order_id")
        @JsonAlias("order_id")
        @JsonProperty("order_id")
        @SerializedName("order_id")
        private String orderId;

        /**
         * 签名后的订单信息
         */
        @JSONField(name = "order_token")
        @JsonAlias("order_token")
        @JsonProperty("order_token")
        @SerializedName("order_token")
        private String orderToken;
    }
}
