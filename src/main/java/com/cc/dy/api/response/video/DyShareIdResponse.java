package com.cc.dy.api.response.video;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 */
@NoArgsConstructor
@Data
public class DyShareIdResponse {
    private static final long serialVersionUID = 1L;

    @JSONField(name = "share_id")
    @JsonAlias("share_id")
    @JsonProperty("share_id")
    @SerializedName("share_id")
    private String shareId;
}
