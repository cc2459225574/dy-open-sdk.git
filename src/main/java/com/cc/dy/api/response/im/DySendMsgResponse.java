package com.cc.dy.api.response.im;

import com.alibaba.fastjson.annotation.JSONField;
import com.cc.dy.api.response.DyResponseStructV4;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 主动发送私信
 * https://open.douyin.com/im/authorize/send/msg
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class DySendMsgResponse extends DyResponseStructV4 {

    /**
     * 描述	消息id
     */
    @JSONField(name = "msg_id")
    @JsonAlias(value = "msg_id")
    @JsonProperty(value = "msg_id")
    @SerializedName(value = "msg_id")
    private String msgId;

}
