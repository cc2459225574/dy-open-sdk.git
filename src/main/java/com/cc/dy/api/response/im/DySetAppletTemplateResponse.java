package com.cc.dy.api.response.im;

import com.alibaba.fastjson.annotation.JSONField;
import com.cc.dy.api.response.DyResponseStructV5;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 创建/更新小程序引导卡片模板
 * https://open.douyin.com/api/douyin/v1/im/set_applet_template
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class DySetAppletTemplateResponse extends DyResponseStructV5 {
    /**
     * 描述	模板 ID
     * 是否必填
     * 示例值	@8hxdhauTCMppanGnM4ltGM780mDqPP+KPpR0qQOmLVAXb/T060zdRmYqig357zEBq6CZRp4NVe6qLIJW/V/x1w==
     */
    @JSONField(name = "card_template_id")
    @JsonAlias(value = "card_template_id")
    @JsonProperty(value = "card_template_id")
    @SerializedName(value = "card_template_id")
    private String cardTemplateId;
}
