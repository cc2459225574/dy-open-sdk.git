package com.cc.dy.api.response.pay;

import com.alibaba.fastjson.annotation.JSONField;
import com.cc.dy.api.response.DyResponseStructV3;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * QueryRefundResponse
 *
 * <p>
 *
 * @author jiangcheng
 * @version 1.0
 * @since 2023/9/4 16:52
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class QueryRefundResponse extends DyResponseStructV3 {
    private RefundInfo refundInfo;

    @Data
    public static class RefundInfo {
        /**
         * 退款金额，单位为分
         */
        @JSONField(name = "refund_amount")
        @JsonAlias("refund_amount")
        @JsonProperty("refund_amount")
        @SerializedName("refund_amount")
        private Long refundAmount;

        /**
         * 退款状态枚举：
         *
         * SUCCESS：成功
         * FAIL：失败
         * PROCESSING：处理中
         */
        @JSONField(name = "refund_status")
        @JsonAlias("refund_status")
        @JsonProperty("refund_status")
        @SerializedName("refund_status")
        private String refundStatus;

        /**
         * 退款时间，Unix 时间戳，10 位，整型数，秒级
         */
        @JSONField(name = "refunded_at")
        @JsonAlias("refunded_at")
        @JsonProperty("refunded_at")
        @SerializedName("refunded_at")
        private Long refundedAt;

        /**
         * 退款账户枚举：
         *
         * TRUE：分账后退款，现金户出款
         * FALSE：分账前退款，在途户出款
         */
        @JSONField(name = "is_all_settled")
        @JsonAlias("is_all_settled")
        @JsonProperty("is_all_settled")
        @SerializedName("is_all_settled")
        private Boolean isAllSettled;

        /**
         * 抖音退款单号
         */
        @JSONField(name = "refund_no")
        @JsonAlias("refund_no")
        @JsonProperty("refund_no")
        @SerializedName("refund_no")
        private String refundNo;

        /**
         * 开发者自定义字段，回调原样回传
         */
        @JSONField(name = "cp_extra")
        @JsonAlias("cp_extra")
        @JsonProperty("cp_extra")
        @SerializedName("cp_extra")
        private String cpExtra;

        /**
         * 退款错误描述
         */
        private String msg;
    }
}
