package com.cc.dy.api.response.link;

import com.alibaba.fastjson.annotation.JSONField;
import com.cc.dy.api.response.DyResponseStructV4;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * createQRCodeV2
 * https://open.douyin.com/api/apps/v1/qrcode/create
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class DyCreateQrcodeResponseV2 extends DyResponseStructV4 {
    /**
     * 是否必填	是
     * 示例值	#######
     */
    @JSONField(name = "img")
    @JsonAlias(value = "img")
    @JsonProperty(value = "img")
    @SerializedName(value = "img")
    private String img;
}
