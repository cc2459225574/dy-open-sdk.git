package com.cc.dy.api.response.user;

import lombok.Data;

/**
 * https://open.douyin.com/platform/doc/6848806527751489550
 *
 * @author Gadfly
 * @since 2021-09-30 14:02
 */
@Data
public class DyUserInfoResponse  {
    private static final long serialVersionUID = 1L;
    /**
     * 用户昵称
     */
    private String nickName;

    /**
     * 用户头像网络地址
     */
    private String avatarUrl;
    /**
     * 性别: * `0` - 未知 * `1` - 男性 * `2` - 女性
     */
    private Integer gender;

    /**
     * 用户所在城市
     */
    private String city;

    /**
     * 用户所在省份
     */
    private String province;

    /**
     * 用户所在国家
     */
    private String country;

    /**
     * 用户 openId
     */
    private String openId;

    /**
     * 敏感数据水印
     */
    private WaterMark watermark;
}
