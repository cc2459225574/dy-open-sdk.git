package com.cc.dy.api.response.link;

import com.alibaba.fastjson.annotation.JSONField;
import com.cc.dy.api.response.DyResponseStructV4;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 查询Schema配额V2
 * https://open.douyin.com/api/apps/v1/url/query_schema_quota
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class DyQuerySchemaQuotaResponseV2 extends DyResponseStructV4 {
    /**
     * 描述	短期有效 schema quota 配置
     * 是否必填	是
     */
    @JSONField(name = "short_term_schema_quota")
    @JsonAlias(value = "short_term_schema_quota")
    @JsonProperty(value = "short_term_schema_quota")
    @SerializedName(value = "short_term_schema_quota")
    private Quota shortTermSchemaQuota;

    /**
     * 描述	长期有效 schema quota 配置
     * 是否必填	是
     */
    @JSONField(name = "long_term_schema_quota")
    @JsonAlias(value = "long_term_schema_quota")
    @JsonProperty(value = "long_term_schema_quota")
    @SerializedName(value = "long_term_schema_quota")
    private Quota longTermSchemaQuota;


    @Data
    public static class Quota implements Serializable {
        /**
         * 描述	schema 已生成次数
         * 是否必填	是
         * 示例值	10
         */
        @JSONField(name = "schema_used")
        @JsonAlias(value = "schema_used")
        @JsonProperty(value = "schema_used")
        @SerializedName(value = "schema_used")
        private Long schemaUsed;

        /**
         * 描述	schema 生成次数上限
         * 是否必填	是
         * 示例值	100000
         */
        @JSONField(name = "schema_limit")
        @JsonAlias(value = "schema_limit")
        @JsonProperty(value = "schema_limit")
        @SerializedName(value = "schema_limit")
        private Long schemaLimit;
    }
}
