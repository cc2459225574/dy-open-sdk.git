package com.cc.dy.api.response;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author chenChao 2023-09-15 15:07
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DyResponseStructV3 extends DyResponse implements Serializable {
    /**
     * 错误码
     */
    @SerializedName(value = "err_no")
    @JSONField(name = "err_no")
    @JsonAlias(value = "err_no")
    @JsonProperty(value = "err_no")
    private Integer errNo;

    /**
     * 错误信息
     */
    @SerializedName(value = "err_tips")
    @JSONField(name = "err_tips")
    @JsonAlias(value = "err_tips")
    @JsonProperty(value = "err_tips")
    private String errTips;
}
