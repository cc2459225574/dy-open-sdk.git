package com.cc.dy.api.response.user;

import lombok.Data;

/**
 * @author chenChao 2023-07-07 16:52
 */
@Data
public class DyMobileResponse {
    /**
     * 用户绑定的手机号（国外手机号会有区号）
     */
    private String phoneNumber;
    /**
     * 没有区号的手机号
     */
    private String purePhoneNumber;
    /**
     * 区号
     */
    private String countryCode;
    /**
     * 水印
     */
    private WaterMark watermark;
}
