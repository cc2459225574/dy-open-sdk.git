package com.cc.dy.api.response;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author chenChao 2023-09-15 15:07
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DyResponseStructV5 extends DyResponse implements Serializable {
    /**
     * 描述	错误码描述
     * 是否必填
     * 示例值
     */
    @JSONField(name = "description")
    @JsonAlias(value = "description")
    @JsonProperty(value = "description")
    @SerializedName(value = "description")
    private String description;

    /**
     * 描述	错误码
     * 是否必填
     * 示例值	0
     */
    @JSONField(name = "error_code")
    @JsonAlias(value = "error_code")
    @JsonProperty(value = "error_code")
    @SerializedName(value = "error_code")
    private Integer errorCode;

    /**
     * 描述	标识请求的唯一 id
     * 是否必填
     * 示例值	202008121419360101980821035705926A
     */
    @JSONField(name = "logid")
    @JsonAlias(value = "logid")
    @JsonProperty(value = "logid")
    @SerializedName(value = "logid")
    private String logid;

    /**
     * 描述	毫秒级时间戳
     * 是否必填
     * 示例值	1597213176393
     */
    @JSONField(name = "now")
    @JsonAlias(value = "now")
    @JsonProperty(value = "now")
    @SerializedName(value = "now")
    private Integer now;

    /**
     * 描述   子错误码描述
     * 是否必填
     * 示例值	sub_error_code
     */
    @JSONField(name = "sub_description")
    @JsonAlias(value = "sub_description")
    @JsonProperty(value = "sub_description")
    @SerializedName(value = "sub_description")
    private String subDescription;

    /**
     * 描述	子错误码
     * 是否必填
     * 示例值 0
     */
    @JSONField(name = "sub_error_code")
    @JsonAlias(value = "sub_error_code")
    @JsonProperty(value = "sub_error_code")
    @SerializedName(value = "sub_error_code")
    private int sub_error_code;


}
