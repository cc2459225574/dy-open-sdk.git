package com.cc.dy.api.response.link;

import com.alibaba.fastjson.annotation.JSONField;
import com.cc.dy.api.response.DyResponseStructV3;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * QueryInfoV2
 * https://open.douyin,com/api/apps/v1/url_link/query_info
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class DyQueryLinkInfoResponseV2 extends DyResponseStructV3 {
    /**
     * 描述	宿主名称，douyin，douyinlite
     * 是否必填	是
     * 示例值	douyin
     */
    @JSONField(name = "app_name")
    @JsonAlias(value = "app_name")
    @JsonProperty(value = "app_name")
    @SerializedName(value = "app_name")
    private String appName;

    /**
     * 描述	小程序ID
     * 是否必填	是
     * 示例值	tt**************
     */
    @JSONField(name = "ma_app_id")
    @JsonAlias(value = "ma_app_id")
    @JsonProperty(value = "ma_app_id")
    @SerializedName(value = "ma_app_id")
    private String maAppId;

    /**
     * 描述	小程序页面路径
     * 是否必填	是
     * 示例值	page/index
     */
    @JSONField(name = "path")
    @JsonAlias(value = "path")
    @JsonProperty(value = "path")
    @SerializedName(value = "path")
    private String path;

    /**
     * 描述	小程序页面query。
     * 是否必填	是
     * 示例值	{xxx:xxxx}
     */
    @JSONField(name = "query")
    @JsonAlias(value = "query")
    @JsonProperty(value = "query")
    @SerializedName(value = "query")
    private String query;

    /**
     * 描述	创建时间，为 Unix 时间戳，单位秒
     * 是否必填	是
     * 示例值	1644463984
     */
    @JSONField(name = "create_time")
    @JsonAlias(value = "create_time")
    @JsonProperty(value = "create_time")
    @SerializedName(value = "create_time")
    private Long createTime;

    /**
     * 描述	到期失效时间，为 Unix 时间戳，单位秒
     * 是否必填	是
     * 示例值	1644464021
     */
    @JSONField(name = "expire_time")
    @JsonAlias(value = "expire_time")
    @JsonProperty(value = "expire_time")
    @SerializedName(value = "expire_time")
    private Long expireTime;
}
