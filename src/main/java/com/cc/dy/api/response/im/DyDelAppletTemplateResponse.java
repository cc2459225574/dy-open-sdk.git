package com.cc.dy.api.response.im;

import com.alibaba.fastjson.annotation.JSONField;
import com.cc.dy.api.response.DyResponseStructV5;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 删除小程序引导卡片模板
 * https://open.douyin.com/api/douyin/v1/im/del_applet_template
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class DyDelAppletTemplateResponse extends DyResponseStructV5 {

}
