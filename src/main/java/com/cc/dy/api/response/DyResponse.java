package com.cc.dy.api.response;

import lombok.Data;

import java.io.Serializable;

/**
 * @author chenChao 2023-09-08 18:45
 */
@Data
public abstract class DyResponse implements Serializable {

//    /**
//     * 是否成功
//     */
//    public abstract boolean isOk();
}
