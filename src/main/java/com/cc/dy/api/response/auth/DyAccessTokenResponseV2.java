package com.cc.dy.api.response.auth;

import com.alibaba.fastjson.annotation.JSONField;
import com.cc.dy.api.response.DyResponseStructV3;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = true)
public class DyAccessTokenResponseV2 extends DyResponseStructV3 implements Serializable {
    /**
     * 获取的 access_token
     */
    @JSONField(name = "access_token")
    @JsonAlias(value = "access_token")
    @JsonProperty(value = "access_token")
    @SerializedName(value = "access_token")
    private String accessToken;

    /**
     * access_token 有效时间，单位：秒
     */
    @JSONField(name = "expires_in")
    @JsonAlias(value = "expires_in")
    @JsonProperty(value = "expires_in")
    @SerializedName(value = "expires_in")
    private Integer expiresIn;
}
