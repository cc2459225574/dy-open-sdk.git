package com.cc.dy.api.response.video;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 */
@NoArgsConstructor
@Data
public class DyVideoCommentCreateResponse {

    private static final long serialVersionUID = 1L;


    @JSONField(name = "comment_id")
    @JsonAlias("comment_id")
    @JsonProperty("comment_id")
    @SerializedName("comment_id")
    private String commentId;

}
