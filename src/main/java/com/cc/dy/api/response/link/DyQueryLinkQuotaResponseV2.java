package com.cc.dy.api.response.link;

import com.alibaba.fastjson.annotation.JSONField;
import com.cc.dy.api.response.DyResponseStructV3;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 查询Link配额
 * https://developer.toutiao.com/api/apps/url_link/query_quota
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class DyQueryLinkQuotaResponseV2 extends DyResponseStructV3 {
    /**
     * 描述	url_link 已生成次数
     * 是否必填	是
     * 示例值	1128
     */
    @JSONField(name = "url_link_used")
    @JsonAlias(value = "url_link_used")
    @JsonProperty(value = "url_link_used")
    @SerializedName(value = "url_link_used")
    private Long urlLinkUsed;

    /**
     * 描述	url_link 生成次数上限
     * 是否必填	是
     * 示例值	1000000
     */
    @JSONField(name = "url_link_limit")
    @JsonAlias(value = "url_link_limit")
    @JsonProperty(value = "url_link_limit")
    @SerializedName(value = "url_link_limit")
    private String urlLinkLimit;

}
