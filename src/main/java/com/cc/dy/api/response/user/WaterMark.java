package com.cc.dy.api.response.user;

import lombok.Data;

import java.io.Serializable;

@Data
public class WaterMark implements Serializable {
    /**
     * 数据源小程序 id
     */
    String appid;
    /**
     * 时间戳，可以用于检查数据的时效性
     */
    String timestamp;
}
