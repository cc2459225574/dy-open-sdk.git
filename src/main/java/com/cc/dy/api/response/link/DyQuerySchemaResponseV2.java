package com.cc.dy.api.response.link;

import com.alibaba.fastjson.annotation.JSONField;
import com.cc.dy.api.response.DyResponseStructV4;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 查询SchemaV2
 * https://open.douyin.com/api/apps/v1/url/query_schema
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class DyQuerySchemaResponseV2 extends DyResponseStructV4 {
    /**
     * 描述	小程序ID
     * 是否必填	是
     * 示例值	tt******
     */
    @JSONField(name = "app_id")
    @JsonAlias(value = "app_id")
    @JsonProperty(value = "app_id")
    @SerializedName(value = "app_id")
    private String appId;

    /**
     * 描述	小程序页面路径。
     * 是否必填	是
     * 示例值	page/index
     */
    @JSONField(name = "path")
    @JsonAlias(value = "path")
    @JsonProperty(value = "path")
    @SerializedName(value = "path")
    private String path;

    /**
     * 描述	小程序页面query。
     * 是否必填	是
     * 示例值	{xxx:xxxx}
     */
    @JSONField(name = "query")
    @JsonAlias(value = "query")
    @JsonProperty(value = "query")
    @SerializedName(value = "query")
    private String query;

    /**
     * 描述	创建时间，为 Unix 时间戳
     * 是否必填	是
     * 示例值	1644463984
     */
    @JSONField(name = "create_time")
    @JsonAlias(value = "create_time")
    @JsonProperty(value = "create_time")
    @SerializedName(value = "create_time")
    private Long createTime;

    /**
     * 描述	到期失效时间，为 Unix 时间戳
     * 是否必填	是
     * 示例值	1644464021
     */
    @JSONField(name = "expire_time")
    @JsonAlias(value = "expire_time")
    @JsonProperty(value = "expire_time")
    @SerializedName(value = "expire_time")
    private Long expireTime;

}
