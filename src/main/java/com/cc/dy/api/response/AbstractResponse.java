package com.cc.dy.api.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户继承
 * @author chenChao 2023-08-28 16:52
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AbstractResponse extends DyResponse {

}
