package com.cc.dy.api.response.link;

import com.alibaba.fastjson.annotation.JSONField;
import com.cc.dy.api.response.DyResponseStructV4;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 生成SchemaV2
 * https://open.douyin.com/api/apps/v1/url/generate_schema
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class DyGenerateSchemaResponseV2 extends DyResponseStructV4 {

    /**
     * 描述      schema链接
     * 是否必填	是
     * 示例值	"sslocal://miniapp?ticket=v1_*******"
     */
    @JSONField(name = "schema")
    @JsonAlias(value = "schema")
    @JsonProperty(value = "schema")
    @SerializedName(value = "schema")
    private String schema;

}
