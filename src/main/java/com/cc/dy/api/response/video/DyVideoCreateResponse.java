package com.cc.dy.api.response.video;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

/**
 *
 */
@Data
public class DyVideoCreateResponse {
    /**
     * 抖音视频id
     */
    @JSONField(name = "item_id")
    @JsonAlias("item_id")
    @JsonProperty("item_id")
    @SerializedName("item_id")
    private String itemId;
}
