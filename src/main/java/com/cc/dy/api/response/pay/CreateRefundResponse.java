package com.cc.dy.api.response.pay;

import com.alibaba.fastjson.annotation.JSONField;
import com.cc.dy.api.response.DyResponseStructV3;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author chenChao 2023-08-28 17:07
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CreateRefundResponse extends DyResponseStructV3 {

    /**
     * 担保交易服务端退款单号
     */
    @JSONField(name = "refund_no")
    @JsonAlias("refund_no")
    @JsonProperty("refund_no")
    @SerializedName("refund_no")
    private String refundNo;
}
