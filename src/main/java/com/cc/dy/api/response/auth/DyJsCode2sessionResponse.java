package com.cc.dy.api.response.auth;

import com.alibaba.fastjson.annotation.JSONField;
import com.cc.dy.api.response.DyResponseStructV3;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class DyJsCode2sessionResponse extends DyResponseStructV3 implements Serializable {
    /**
     * 会话密钥，如果请求时有 code 参数才会返回
     */
    @JSONField(name = "session_key")
    @JsonAlias("session_key")
    @JsonProperty("session_key")
    @SerializedName("session_key")
    String sessionKey;

    /**
     * 用户在当前小程序的 ID，如果请求时有 code 参数才会返回
     */
    @JSONField(name = "openid")
    @JsonAlias("openid")
    @JsonProperty("openid")
    @SerializedName("openid")
    String openid;

    /**
     * 匿名用户在当前小程序的 ID，如果请求时有 anonymous_code 参数才会返回
     */
    @JSONField(name = "anonymous_openid")
    @JsonAlias("anonymous_openid")
    @JsonProperty("anonymous_openid")
    @SerializedName("anonymous_openid")
    String anonymousOpenid;

    /**
     * 用户在小程序平台的唯一标识符，请求时有 code 参数才会返回。如果开发者拥有多个小程序，可通过 unionid 来区分用户的唯一性。
     */
    @JSONField(name = "unionid")
    @JsonAlias("unionid")
    @JsonProperty("unionid")
    @SerializedName("unionid")
    String unionid;
}
