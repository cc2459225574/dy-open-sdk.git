package com.cc.dy.api.response.auth;

import com.alibaba.fastjson.annotation.JSONField;
import com.cc.dy.api.response.DyResponseStructV2;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 获取用户授权调用凭证
 * https://open.douyin.com/oauth/access_token
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class DyAccessTokenResponse extends DyResponseStructV2 {

    /**
     * 描述	access_token接口调用凭证超时时间，单位（秒)
     * 示例值	86400
     */
    @JSONField(name = "expires_in")
    @JsonAlias(value = "expires_in")
    @JsonProperty(value = "expires_in")
    @SerializedName(value = "expires_in")
    private Long expiresIn;

    /**
     * 描述	授权用户唯一标识
     * 示例值	SuQB***KN5f
     */
    @JSONField(name = "open_id")
    @JsonAlias(value = "open_id")
    @JsonProperty(value = "open_id")
    @SerializedName(value = "open_id")
    private String openId;

    /**
     * 描述	refresh_token凭证超时时间，单位（秒)
     * 示例值	86400
     */
    @JSONField(name = "refresh_expires_in")
    @JsonAlias(value = "refresh_expires_in")
    @JsonProperty(value = "refresh_expires_in")
    @SerializedName(value = "refresh_expires_in")
    private Long refreshExpiresIn;

    /**
     * 描述	用户刷新access_token
     * 示例值	refresh_token
     */
    @JSONField(name = "refresh_token")
    @JsonAlias(value = "refresh_token")
    @JsonProperty(value = "refresh_token")
    @SerializedName(value = "refresh_token")
    private String refreshToken;

    /**
     * 描述	用户授权的作用域(Scope)，使用逗号（,）分隔，开放平台几乎每个接口都需要特定的Scope。
     * 示例值	user_info
     */
    @JSONField(name = "scope")
    @JsonAlias(value = "scope")
    @JsonProperty(value = "scope")
    @SerializedName(value = "scope")
    private String scope;

    /**
     * 描述	接口调用凭证
     * 示例值	access_token
     */
    @JSONField(name = "access_token")
    @JsonAlias(value = "access_token")
    @JsonProperty(value = "access_token")
    @SerializedName(value = "access_token")
    private String accessToken;

}
