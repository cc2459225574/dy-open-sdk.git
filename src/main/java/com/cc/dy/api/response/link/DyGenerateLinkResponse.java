package com.cc.dy.api.response.link;

import com.alibaba.fastjson.annotation.JSONField;
import com.cc.dy.api.response.DyResponseStructV3;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * GenerateV2
 * https://open.douyin.com/api/apps/v1/url_link/generate
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class DyGenerateLinkResponse extends DyResponseStructV3 {
    /**
     * 生成的url
     */
    @SerializedName(value = "url_link")
    @JSONField(name = "url_link")
    @JsonAlias(value = "url_link")
    @JsonProperty(value = "url_link")
    private Integer urlLink;
}
