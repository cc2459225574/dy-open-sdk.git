package com.cc.dy.api.response.pay;

import com.alibaba.fastjson.annotation.JSONField;
import com.cc.dy.api.response.DyResponseStructV3;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author chenChao 2023-08-28 16:39
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class QueryOrderResponse extends DyResponseStructV3 {
    /**
     * 开发者侧的订单号
     */
    @JSONField(name = "out_order_no")
    @JsonAlias("out_order_no")
    @JsonProperty("out_order_no")
    @SerializedName("out_order_no")
    private String outOrderNo;
    /**
     * 抖音侧的订单号
     */
    @JSONField(name = "order_id")
    @JsonAlias("order_id")
    @JsonProperty("order_id")
    @SerializedName("order_id")
    private String orderId;
    /**
     * 支付信息
     */
    @JSONField(name = "payment_info")
    @JsonAlias("payment_info")
    @JsonProperty("payment_info")
    @SerializedName("payment_info")
    private PaymentInfo paymentInfo;
    /**
     * 若该订单为CPS订单，该字段会返回该笔订单的CPS相关信息
     */
    @JSONField(name = "cps_info")
    @JsonAlias("cps_info")
    @JsonProperty("cps_info")
    @SerializedName("cps_info")
    private CpsInfo cpsInfo;

    @Data
    public static class PaymentInfo implements Serializable {
        /**
         * 支付金额，单位为分
         */
        @JSONField(name = "total_fee")
        @JsonAlias("total_fee")
        @JsonProperty("total_fee")
        @SerializedName("total_fee")
        private Long totalFee;
        /**
         * 支付状态枚举值：
         * SUCCESS：成功 TIMEOUT：超时未支付 PROCESSING：处理中 FAIL：失败
         */
        @JSONField(name = "order_status")
        @JsonAlias("order_status")
        @JsonProperty("order_status")
        @SerializedName("order_status")
        private String orderStatus;
        /**
         * 支付完成时间，order_status不为SUCCESS时会返回默认值空字符串，order_status为SUCCESS时返回非空字符串，格式为"yyyy-MM-dd HH:mm:ss"
         */
        @JSONField(name = "pay_time")
        @JsonAlias("pay_time")
        @JsonProperty("pay_time")
        @SerializedName("pay_time")
        private String payTime;
        /**
         * 支付渠道，order_status不为SUCCESS时会返回默认值0，order_status为SUCCESS时会返回以下枚举：
         * 1-微信支付，
         * 2-支付宝支付，
         * 10-抖音支付
         */
        @JSONField(name = "way")
        @JsonAlias("way")
        @JsonProperty("way")
        @SerializedName("way")
        private Long way;
        /**
         * 支付渠道侧的支付单号
         */
        @JSONField(name = "channel_no")
        @JsonAlias("channel_no")
        @JsonProperty("channel_no")
        @SerializedName("channel_no")
        private String channelNo;
        /**
         * 该笔交易卖家商户号
         */
        @JSONField(name = "seller_uid")
        @JsonAlias("seller_uid")
        @JsonProperty("seller_uid")
        @SerializedName("seller_uid")
        private String sellerUid;
        /**
         * 订单来源视频对应视频 id
         */
        @JSONField(name = "item_id")
        @JsonAlias("item_id")
        @JsonProperty("item_id")
        @SerializedName("item_id")
        private String itemId;
        /**
         * 开发者自定义字段
         */
        @JSONField(name = "cp_extra")
        @JsonAlias("cp_extra")
        @JsonProperty("cp_extra")
        @SerializedName("cp_extra")
        private String cpExtra;
    }

    @Data
    public static class CpsInfo implements Serializable {

        /**
         * 达人分佣金额，单位为分。后续商户在进行分账时需要注意可分账金额应扣除达人分佣金额。
         * 注意：由于订单归因与佣金计算存在延迟，支付成功后立即查询可能未计算完成，建议开发者在支付成功后分账前再进行查询。
         */
        @JSONField(name = "share_amount")
        @JsonAlias("share_amount")
        @JsonProperty("share_amount")
        @SerializedName("share_amount")
        private String shareAmount;
        /**
         * 达人抖音号
         */
        @JSONField(name = "douyin_id")
        @JsonAlias("douyin_id")
        @JsonProperty("douyin_id")
        @SerializedName("douyin_id")
        private String douyinId;
        /**
         * 达人昵称
         */
        @JSONField(name = "nickname")
        @JsonAlias("nickname")
        @JsonProperty("nickname")
        @SerializedName("nickname")
        private String nickname;
    }
}
