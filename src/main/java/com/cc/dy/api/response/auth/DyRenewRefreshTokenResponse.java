package com.cc.dy.api.response.auth;

import com.alibaba.fastjson.annotation.JSONField;
import com.cc.dy.api.response.DyResponseStructV2;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 刷新授权调用凭证
 * https://open.douyin.com/oauth/renew_refresh_token/沙盒环境：https://open-sandbox.douyin.com/oauth/renew_refresh_token
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class DyRenewRefreshTokenResponse extends DyResponseStructV2 {

    /**
     * 描述	过期时间，单位（秒）
     * 示例值	86400
     */
    @JSONField(name = "expires_in")
    @JsonAlias(value = "expires_in")
    @JsonProperty(value = "expires_in")
    @SerializedName(value = "expires_in")
    private Long expiresIn;

    /**
     * 描述	用户刷新
     * 示例值	refresh_token
     */
    @JSONField(name = "refresh_token")
    @JsonAlias(value = "refresh_token")
    @JsonProperty(value = "refresh_token")
    @SerializedName(value = "refresh_token")
    private String refreshToken;
}
