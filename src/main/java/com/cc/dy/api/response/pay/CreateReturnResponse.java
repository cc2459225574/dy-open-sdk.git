package com.cc.dy.api.response.pay;

import com.alibaba.fastjson.annotation.JSONField;
import com.cc.dy.api.response.DyResponseStructV3;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * CreateReturnResponse
 *
 * <p>
 *
 * @author jiangcheng
 * @version 1.0
 * @since 2023/9/4 17:44
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CreateReturnResponse extends DyResponseStructV3 {
    /**
     * 分账回退基本信息
     */
    @JSONField(name = "return_info")
    @JsonAlias("return_info")
    @JsonProperty("return_info")
    @SerializedName("return_info")
    private ReturnInfo returnInfo;

    @Data
    public static class ReturnInfo {

        /**
         * 小程序 ID
         */
        @JSONField(name = "app_id")
        @JsonAlias("app_id")
        @JsonProperty("app_id")
        @SerializedName("app_id")
        private String appId;

        /**
         * 第三方平台服务商 id，非服务商模式留空
         */
        @JSONField(name = "thirdparty_id")
        @JsonAlias("thirdparty_id")
        @JsonProperty("thirdparty_id")
        @SerializedName("thirdparty_id")
        private String thirdPartyId;

        /**
         * 开发者侧的结算号,与out_settle_no二选一填写
         */
        @JSONField(name = "settle_no")
        @JsonAlias("settle_no")
        @JsonProperty("settle_no")
        @SerializedName("settle_no")
        private Long settleNo;

        /**
         * 商户侧的结算号，与settle_no二选一填写
         */
        @JSONField(name = "out_settle_no")
        @JsonAlias("out_settle_no")
        @JsonProperty("out_settle_no")
        @SerializedName("out_settle_no")
        private String outSettleNo;

        /**
         * 开发者回退单号，相同回退单号小程序平台会进行幂等处理。 只能使用数字、大小写字母_-*。
         */
        @JSONField(name = "out_return_no")
        @JsonAlias("out_return_no")
        @JsonProperty("out_return_no")
        @SerializedName("out_return_no")
        private String outReturnNo;

        /**
         * 回退商户号，即原分账方商户号
         */
        @JSONField(name = "merchant_uid")
        @JsonAlias("merchant_uid")
        @JsonProperty("merchant_uid")
        @SerializedName("merchant_uid")
        private String merchantUid;

        /**
         * 回退金额，单位分，取值范围：[1,10000000000]
         */
        @JSONField(name = "return_amount")
        @JsonAlias("return_amount")
        @JsonProperty("return_amount")
        @SerializedName("return_amount")
        private Long returnAmount;

        /**
         * 平台回退单号
         */
        @JSONField(name = "平台回退单号")
        @JsonAlias("return_no")
        @JsonProperty("return_no")
        @SerializedName("return_no")
        private String returnNo;

        /**
         * 回退状态成功-SUCCESS失败-FAIL处理中-PROCESSING
         */
        @JSONField(name = "return_status")
        @JsonAlias("return_status")
        @JsonProperty("return_status")
        @SerializedName("return_status")
        private String returnStatus;

        /**
         * 回退失败的原因，此字段仅回退结果为FAIL时存在
         */
        @JSONField(name = "fail_reason")
        @JsonAlias("fail_reason")
        @JsonProperty("fail_reason")
        @SerializedName("fail_reason")
        private String failReason;

        /**
         * 回退完成时间，Unix 时间戳，单位秒，10 位，整型数
         */
        @JSONField(name = "finish_time")
        @JsonAlias("finish_time")
        @JsonProperty("finish_time")
        @SerializedName("finish_time")
        private Long finishTime;

        /**
         * 用户附加信息
         */
        @JSONField(name = "cp_extra")
        @JsonAlias("cp_extra")
        @JsonProperty("cp_extra")
        @SerializedName("cp_extra")
        private String cpExtra;
    }
}
