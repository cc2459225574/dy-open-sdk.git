package com.cc.dy.api.response.im;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.experimental.Accessors;
/**
*图片上传
*https://open.douyin.com/tool/imagex/client_upload*/
@Data
@Accessors(chain = true)
public class DyClientUploadResponse {
}
