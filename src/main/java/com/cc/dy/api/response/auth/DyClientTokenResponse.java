package com.cc.dy.api.response.auth;

import com.alibaba.fastjson.annotation.JSONField;
import com.cc.dy.api.response.DyResponseStructV2;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 获取应用授权调用凭证
 * POST https://open.douyin.com/oauth/client_token
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class DyClientTokenResponse extends DyResponseStructV2 {
    /**
     * 描述	client_token 接口调用凭证超时时间，单位（秒）
     * 示例值	86400
     */
    @JSONField(name = "expires_in")
    @JsonAlias(value = "expires_in")
    @JsonProperty(value = "expires_in")
    @SerializedName(value = "expires_in")
    private Long expiresIn;

    /**
     * 描述	client_token 接口调用凭证
     * 示例值	示例：clt.5a14a88ef6ebcdc4688224YC8dCJKLPZ******
     */
    @JSONField(name = "access_token")
    @JsonAlias(value = "access_token")
    @JsonProperty(value = "access_token")
    @SerializedName(value = "access_token")
    private String accessToken;
}
