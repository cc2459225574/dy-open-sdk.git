package com.cc.dy.api.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户泛化
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DyBaseResponse extends DyResponse {

}
