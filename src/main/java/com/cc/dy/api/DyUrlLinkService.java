package com.cc.dy.api;

import com.cc.dy.api.request.link.DyCreateQrcodeRequestV2;
import com.cc.dy.api.request.link.DyGenerateLinkRequest;
import com.cc.dy.api.request.link.DyGenerateLinkRequestV2;
import com.cc.dy.api.request.link.DyGenerateSchemaRequestV2;
import com.cc.dy.api.request.link.DyQueryLinkInfoRequestV2;
import com.cc.dy.api.request.link.DyQueryLinkQuotaRequestV2;
import com.cc.dy.api.request.link.DyQuerySchemaQuotaRequestV2;
import com.cc.dy.api.request.link.DyQuerySchemaRequestV2;
import com.cc.dy.api.response.link.DyCreateQrcodeResponseV2;
import com.cc.dy.api.response.link.DyGenerateLinkResponse;
import com.cc.dy.api.response.link.DyGenerateLinkResponseV2;
import com.cc.dy.api.response.link.DyGenerateSchemaResponseV2;
import com.cc.dy.api.response.link.DyQueryLinkInfoResponseV2;
import com.cc.dy.api.response.link.DyQueryLinkQuotaResponseV2;
import com.cc.dy.api.response.link.DyQuerySchemaQuotaResponseV2;
import com.cc.dy.api.response.link.DyQuerySchemaResponseV2;

/**
 * @author chenChao 2023-09-15 16:28
 */
public interface DyUrlLinkService {
    /**
     * 该接口用于生成能够直接在端内跳转小程序的 schema。
     * 使用限制
     * 该接口设有频次限制，如有特殊需求请 oncall。
     * 短期有效：每日上限不超过50万条，qps最高100
     * 长期有效：配置上限不超过10万条，qps最高100。
     * 接口说明
     * 根据参数生成 schema 链接，在端内跳转到指定小程序页面。
     * 1.长期有效 schema，可通过no_expire指定，生成后可永久使用。
     * 2.短期有效 schema，需传入到期时间的 Unix 时间戳，最长距离当前时间 180 天。
     * 权限要求
     * 需要小程序转正
     * 请求示例
     * curl --location 'https://developer.toutiao.com/api/apps/v1/url/generate_schema' \
     * --header 'Access-Token: 0801********3d3d' \
     * --header 'Content-Type: application/json' \
     * --data '{
     * "app_id":"tt5da*****",
     * "query":"{\"key1\":\"val1\",\"key2\":\"val2\"}",
     * "path":"test_miniapp",
     * "no_expire":false,
     * "expire_time":1685090014
     * }'
     */
    DyGenerateSchemaResponseV2 generateSchemaV2(DyGenerateSchemaRequestV2 dyGenerateSchemaRequestV2);

    /**
     * 该接口用于查询已经生成的 schema 的信息。
     * 使用限制
     * 无
     * 接口说明
     * 查询已经生成的 schema 的信息，过期的 schema 返回 12023(schema 不存在或已失效)。
     * 权限要求
     * 需要小程序转正
     * 请求示例
     * curl --location 'https://developer.toutiao.com/api/apps/v1/url/query_schema' \
     * --header 'Access-Token: 0801*********3d3d' \
     * --header 'Content-Type: application/json' \
     * --data '{
     * "schema":"sslocal://miniapp?ticket=v1_*****",
     * "app_id":"tt*****"
     * }
     * '
     */
    DyQuerySchemaResponseV2 querySchemaV2(DyQuerySchemaRequestV2 dyQuerySchemaRequestV2);

    /**
     * 该接口用于查询当前小程序的 schema 配额。
     * 使用限制
     * 无
     * 接口说明
     * 查询当前小程序生成 schema 链接的配额，链接生成方式见生成Schema。
     * 权限要求
     * 需要小程序转正
     * 请求示例
     * curl --location 'https://developer.toutiao.com/api/apps/v1/url/query_schema_quota' \
     * --header 'Access-Token: 0801***************3d3d' \
     * --header 'Content-Type: application/json' \
     * --data '{
     * "app_id":"tt********"
     * }'
     */
    DyQuerySchemaQuotaResponseV2 querySchemaQuotaV2(DyQuerySchemaQuotaRequestV2 dyQuerySchemaQuotaRequestV2);

    /**
     * 该接口用于生成能够从端外跳转到指定宿主小程序的 URL Link。
     * 使用限制
     * 接口有频次限制，每个小程序，100 QPS，50w 次/自然日。如有特殊需求联系 Oncall。
     * 接口说明
     * 根据参数生成 url link 链接，需传入到期时间的 Unix 时间戳，最长距离当前时间 180 天。
     * 1.实际有效时间按照小时计算，不满一小时自动向上取整。
     * 假设当前时间（ct）为2022-2-2 16:23，传入到期时间（et）为 2022-2-2 18:00
     * et-ct = 一小时37分钟，向上取整为2小时
     * 因此，实际到期时间为2小时，即2022-2-2 18:23过期
     * 权限要求
     * 需要小程序转正
     * 请求示例
     * curl --location --request POST 'https://developer.toutiao.com/api/apps/url_link/generate' \
     * --header 'Content-Type: application/json' \
     * --data-raw '{
     * "access_token":"0801121***********",
     * "ma_app_id":"tt***********",
     * "app_name":"douyin",
     * "path":"xxxx",
     * "query":"{xxx:xxxx}",
     * "expire_time":1644464021
     * }'
     */
    DyGenerateLinkResponse generateLink(DyGenerateLinkRequest dyGenerateLinkRequest);

    /**
     * 该接口用于生成能够直接跳转到端内小程序的 url link。
     * 使用限制
     * 接口有频次限制，每个小程序，100qps，50w 次/自然日。如有特殊需求联系 oncall。
     * 接口说明
     * 根据参数生成 url link 链接，需传入到期时间的 Unix 时间戳，最长距离当前时间 180 天。
     * 1.实际有效时间按照小时计算，不满一小时自动向上取整。
     * 假设当前时间（ct）为2022-2-2 16:23，传入到期时间（et）为 2022-2-2 18:00
     * et-ct = 一小时37分钟，向上取整为2小时
     * 因此，实际到期时间为2小时，即2022-2-2 18:23过期
     * 权限要求
     * 需要小程序转正
     * 请求示例
     * 根据典型使用场景或请求参数表格中的示例值，编写请求示例。 如果包含多个请求示例， 使用无序列表样式分别展示。 在每个示例前，需要以正文样式描述该示例的场景或作用。
     * curl --location --request POST 'https://open.douyin.com/api/apps/v1/url_link/generate' \
     * --header 'Content-Type: application/json' \
     * --header 'access-token: clt.943da17996fb5cebfbc70c044c3fc25a57T54DcjT6HNKGqnUdxzy1******'
     * --data-raw '{
     * "app_id":"tt********",
     * "app_name":"douyin",
     * "path":"xxxx",
     * "query":"{xxx:xxxx}",
     * "expire_time":1644464021,
     * }'
     * 响应示例
     * 正常示例
     * {
     * "err_no": 0,
     * "err_msg": "",
     * "data": {
     * "url_link": "https://z.douyin.com/Lu86lrX"
     * }
     * }
     * 异常示例
     * {
     * "err_no": 28005001,
     * "err_msg": "参数解析失败"
     * }
     * 需要以表格形式提供完整的错误码信息，表格中必须包含 HTTP 状态码、错误码、描述 和 排查建议 4 项。 描述在代码中是英文，这里建议换行补充对应的中文描述，方便开发者理解。
     */
    DyGenerateLinkResponseV2 generateLinkV2(DyGenerateLinkRequestV2 dyGenerateLinkRequestV2);


    /**
     * 该接口用于查询当前小程序配额。
     * 使用限制
     * 无
     * 接口说明
     * 查询当前小程序生成 url link 链接的配额，链接生成方式见生成Link。
     * 权限要求
     * 需要小程序转正
     * 请求示例
     * curl --location --request POST 'https://developer.toutiao.com/api/apps/url_link/query_quota' \
     * --header 'Content-Type: application/json' \
     * --data-raw '{
     * "access_token":"0801121***********",
     * "ma_app_id":"tt******"
     * }'
     * 正常示例
     * {
     * "err_no": 0,
     * "err_msg": "success",
     * "url_link_quota": {
     * "url_link_used": 1128,
     * "url_link_limit": 1000000
     * }
     * }
     * 异常示例
     * {
     * "err_no": 28005001,
     * "err_tips": "参数填写错误"
     * }
     */
    DyQueryLinkQuotaResponseV2 queryLinkQuotaV2(DyQueryLinkQuotaRequestV2 dyQueryLinkQuotaRequestV2);

    /**
     * 该接口用于查询已经生成的 link 的信息。
     * 使用限制
     * 无
     * 接口说明
     * 查询已经生成的 link 的信息，过期的 url_link 返回"url_link 不存在"。
     * 权限要求
     * 需要小程序转正
     * 请求示例
     * curl --location --request POST 'https://open.douyin.com/api/apps/v1/url_link/query_info' \
     * --header 'Content-Type: application/json' \
     * --header 'access-token: clt.943da17996fb5cebfbc70c044c3fc25a57T54DcjT6HNKGqnUdxzy1******'
     * --data-raw '{
     * "app_id":"tt***********",
     * "url_link":"https://z.douyin.com/Lu86lrX"
     * }'
     * 响应示例
     * 正常示例
     * {
     * "err_no": 0,
     * "err_msg": "",
     * "data": {
     * "app_name": "douyin",
     * "app_id": "tt6f31198cf00b5xxx",
     * "path": "xxxx",
     * "query": "xxxx",
     * "create_time": 1644463984,
     * "expire_time": 1644464021
     * }
     * }
     * 异常示例
     * {
     * "err_no": 12023,
     * "err_msg": "URL_Link不存在或已失效"
     * }
     */
    DyQueryLinkInfoResponseV2 queryLinkInfoV2(DyQueryLinkInfoRequestV2 dyQueryLinkInfoRequestV2);

    /**
     * 获取小程序/小游戏的二维码。该二维码可通过任意 app 扫码打开，能跳转到开发者指定的对应字节系 app 内拉起小程序/小游戏， 并传入开发者指定的参数。通过该接口生成的二维码，永久有效，暂无数量限制。
     * 使用限制
     * 无
     * 接口说明
     * 开发者调用来生成服务端分享的小程序二维码
     * 权限要求
     * 请求示例
     * curl --location --request POST 'https://open.douyin.com/api/apps/v1/qrcode/create/' \
     * --header 'Content-Type: application/json' \
     * --header 'access-token: clt.943da17996fb5cebfbc70c044c3fc25a57T54DcjT6HNKGqnUdxzy1******'
     * --data-raw '{
     * "app_name": "douyin",
     * "appid": "tt**********",
     * "path": "{}",
     * "width": 1000,
     * "line_color": {
     * "r": 0,
     * "g": 0,
     * "b": 0
     * },
     * "background": {
     * "r": 0,
     * "g": 0,
     * "b": 0
     * },
     * "set_icon": false
     * }'
     * 响应示例
     * 正常示例
     * {
     * "err_no": 0,
     * "err_msg": "",
     * "data": {
     * "img": "#######"
     * }
     * }
     * 异常示例
     * {
     * "err_no": 28005001,
     * "err_msg": "参数解析失败"
     * }
     * 需要以表格形式提供完整的错误码信息，表格中必须包含 HTTP 状态码、错误码、描述 和 排查建议 4 项。 描述在代码中是英文，这里建议换行补充对应的中文描述，方便开发者理解。
     */
    DyCreateQrcodeResponseV2 createQrcodeV2(DyCreateQrcodeRequestV2 dyCreateQrcodeRequestV2);
}
