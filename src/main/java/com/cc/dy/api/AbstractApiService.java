package com.cc.dy.api;

import com.cc.dy.api.response.DyResponse;
import com.cc.dy.api.response.DyResponseStructV2;
import com.cc.dy.enums.EnvEnum;
import com.cc.dy.executer.DyService;
import lombok.AllArgsConstructor;

import java.util.HashMap;
import java.util.Map;

/**
 * @author chenChao 2023-09-04 11:02
 */
@AllArgsConstructor
public abstract class AbstractApiService {
    protected DyService dyService;

    protected String getApiUrl(String url) {
        EnvEnum envEnum = dyService.getEnv();
        return envEnum.getUrl() + url;
    }

    protected String getAccessToken() {
        return dyService.getDyAuthService().getAccessToken();
    }

    protected Map<String, String> getAccessTokenMap() {
        Map<String, String> headerMap = new HashMap<>();
        String accessToken = getAccessToken();
        headerMap.put("access-token", accessToken);
        headerMap.put("access_token", accessToken);
        headerMap.put("Content-Type", "application/json");
        return headerMap;
    }

    /**
     * 当本Service没有实现某个API的时候，可以用这个，针对所有API中的POST请求.
     *
     * @param t       返回对象的类型
     * @param url     请求接口地址
     * @param request 请求对象
     */
    protected <T extends DyResponse> T post(String url, Object request, Class<T> t) {
        return dyService.getDyHttpClient().post(getApiUrl(url), request, t);
    }

    /**
     * 当本Service没有实现某个API的时候，可以用这个，针对所有API中的POST请求.
     *
     * @param t       返回对象的类型
     * @param url     请求接口地址
     * @param request 请求对象
     */
    protected <T extends DyResponse> T postWithAccessToken(String url, Object request, Class<T> t) {
        return dyService.getDyHttpClient().postWithHeaders(getApiUrl(url), getAccessTokenMap(), request, t);
    }

    /**
     * POST请求
     *
     * @param url        请求接口地址
     * @param urlRequest 请求地址参数
     * @param request    请求对象
     * @param t          类型
     * @return {@link T}
     */
    protected <T extends DyResponse> T postWithAccessToken(String url, Map<String, Object> urlRequest, Object request, Class<T> t) {
        return dyService.getDyHttpClient().postWithHeaders(getApiUrl(url), getAccessTokenMap(), urlRequest, request, t);
    }

}
