package com.cc.dy.http.impl;

import cn.hutool.core.collection.CollUtil;
import com.cc.dy.api.response.DyResponse;
import com.cc.dy.executer.DyService;
import com.cc.dy.http.AbstractDyHttpClient;
import com.cc.dy.json.JsonSerializer;
import com.cc.dy.json.impl.FastJsonSerializer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

@Slf4j
public class RestTemplateDyHttpClient extends AbstractDyHttpClient {

    private final RestTemplate restTemplate;

    public RestTemplateDyHttpClient(DyService dyService) {
        super(dyService, new FastJsonSerializer());
        this.restTemplate = new RestTemplate();
    }

    public RestTemplateDyHttpClient(DyService dyService, JsonSerializer jsonSerializer) {
        super(dyService, jsonSerializer);
        this.restTemplate = new RestTemplate();
    }


    /**
     * GET请求
     *
     * @param url     请求地址
     * @param headers 请求头
     * @param t       返回对象的类型
     * @return {@link T}
     */
    @Override
    public <T extends DyResponse> T getWithHeaders(String url, Map<String, String> headers, Class<T> t) {
        HttpHeaders httpHeaders = new HttpHeaders();
        if (headers != null) {
            for (String headerName : headers.keySet()) {
                httpHeaders.add(headerName, headers.get(headerName));
            }
        }
        HttpEntity<Void> httpEntity = new HttpEntity<>(httpHeaders);
        log.info("GET请求字节跳动接口,请求地址: 【{}】, 请求头: 【{}】", url, headers);
        ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class, Collections.emptyMap());
        log.info("请求字节跳动接口返回数据: 【{}】", getJsonSerializer().toJson(result));
        return this.getJsonSerializer().parseResponse(result.getBody(), t);
    }


    /**
     * POST请求
     *
     * @param url        请求接口地址
     * @param headers    请求头
     * @param urlRequest 请求地址参数
     * @param request    请求对象
     * @param t          类型
     * @return {@link T}
     */
    @Override
    public <T extends DyResponse> T postWithHeaders(String url, Map<String, String> headers, Map<String, Object> urlRequest, Object request, Class<T> t) {
        HttpHeaders httpHeaders = new HttpHeaders();
        if (Objects.nonNull(headers)) {
            for (String headerName : headers.keySet()) {
                httpHeaders.add(headerName, headers.get(headerName));
            }
        }
        Object httpEntity;
        if (httpHeaders.getContentType() == null) {
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpEntity = new HttpEntity<>(getJsonSerializer().toJson(request), httpHeaders);
        } else {
            Map<String, Object> param = handlerRequestParam(request);
            httpEntity = new HttpEntity<>(param, httpHeaders);
        }
        // url参数
        if (CollUtil.isNotEmpty(urlRequest)) {
            url = urlJoinParams(url, urlRequest);
        }
        log.info("POST请求字节跳动接口,请求地址: 【{}】, 请求头: 【{}】, 请求参数: 【{}】", url, headers, request);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, httpEntity, String.class);
        log.info("请求字节跳动接口返回数据: 【{}】", getJsonSerializer().toJson(responseEntity));
        return this.getJsonSerializer().parseResponse(responseEntity.getBody(), t);
    }

}
