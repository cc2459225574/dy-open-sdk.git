package com.cc.dy.http;

import com.cc.dy.api.response.DyResponse;
import com.cc.dy.json.JsonSerializer;

import java.util.Map;

public interface DyHttpClient {

    JsonSerializer getJsonSerializer();


    /**
     * GET请求
     *
     * @param t   返回对象的类型
     * @param url 请求地址
     * @return {@link T}
     */
    <T extends DyResponse> T get(String url, Class<T> t);

    /**
     * GET请求
     *
     * @param t       返回对象的类型
     * @param url     请求地址
     * @param headers 请求头
     * @return {@link T}
     */
    <T extends DyResponse> T getWithHeaders(String url, Map<String, String> headers, Class<T> t);


    /**
     * 当本Service没有实现某个API的时候，可以用这个，针对所有API中的POST请求.
     *
     * @param t       返回对象的类型
     * @param url     请求接口地址
     * @param request 请求对象
     * @return {@link T}
     */
    <T extends DyResponse> T post(String url, Object request, Class<T> t);

    /**
     * POST请求
     *
     * @param url     请求接口地址
     * @param headers 请求头
     * @param request 请求对象
     * @param t       类型
     * @return {@link T}
     */
    <T extends DyResponse> T postWithHeaders(String url, Map<String, String> headers, Object request, Class<T> t);

    /**
     * POST请求
     *
     * @param url        请求接口地址
     * @param headers    请求头
     * @param urlRequest 请求地址参数
     * @param request    请求对象
     * @param t          类型
     * @return {@link T}
     */
    <T extends DyResponse> T postWithHeaders(String url, Map<String, String> headers, Map<String, Object> urlRequest, Object request, Class<T> t);

}
