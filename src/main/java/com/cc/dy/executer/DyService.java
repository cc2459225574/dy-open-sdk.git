package com.cc.dy.executer;

import com.cc.dy.api.DyAuthService;
import com.cc.dy.api.DyPayService;
import com.cc.dy.api.DyUserService;
import com.cc.dy.config.DyConfig;
import com.cc.dy.enums.EnvEnum;
import com.cc.dy.http.DyHttpClient;
import com.cc.dy.storage.StorageService;


public interface DyService {
    /**
     * 切换小程序
     *
     * @param clientId 小程序id
     */
    void switchClient(String clientId);

    /**
     * 添加小程序配置信息
     *
     * @param dyConfig 小程序配置信息
     */
    void addDyConfig(DyConfig dyConfig);

    /**
     * 获取当前小程序配置
     *
     * @return 小程序配置
     */
    DyConfig getDyConfig();

    /**
     * 设置当前环境
     *
     * @param env 环境
     */
    void setEnv(EnvEnum env);

    /**
     * 获取当前环境
     *
     * @return 环境
     */
    EnvEnum getEnv();

    /**
     * 设置请求对象
     *
     * @param dyHttpClient 请求对象
     */
    void setDyHttpClient(DyHttpClient dyHttpClient);

    /**
     * 获取请求对象
     *
     * @return 请求对象
     */
    DyHttpClient getDyHttpClient();

    /**
     * 设置缓存对象
     *
     * @param storageService 缓存对象
     */
    void setStorageService(StorageService storageService);

    /**
     * 获取缓存对象
     *
     * @return 缓存对象
     */
    StorageService getStorageService();

    /**
     * 登录认证
     *
     * @return 登录认证服务
     */
    DyAuthService getDyAuthService();

    /**
     * 用户信息
     *
     * @return 用户信息服务
     */
    DyUserService getDyUserService();

    /**
     * 支付服务
     *
     * @return 支付服务
     */
    DyPayService getDyPayService();
}
