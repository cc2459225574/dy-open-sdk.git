package com.cc.dy.executer.impl;

import com.cc.dy.executer.AbstractDyService;
import org.springframework.data.redis.core.RedisTemplate;

public class DefaultDyServiceImpl extends AbstractDyService {
    public DefaultDyServiceImpl() {
    }

    public DefaultDyServiceImpl(RedisTemplate<String, String> redisTemplate) {
        super(redisTemplate);
    }

}
