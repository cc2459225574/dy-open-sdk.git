package com.cc.dy.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author chenChao 2023-09-04 9:52
 */
@Getter
@AllArgsConstructor
public enum EnvEnum {
    /**
     * 生产环境
     */
    PRODUCTION("https://developer.toutiao.com"),
    /**
     * 沙盒环境
     */
    SANDBOX("https://open-sandbox.douyin.com"),
    ;

    private final String url;
}
