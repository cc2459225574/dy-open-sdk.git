package com.cc.dy.enums;

import com.cc.dy.exception.DyException;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.function.Supplier;

/**
 * @author chenChao 2023-09-04 10:14
 */
@Getter
@AllArgsConstructor
public enum DyErrorCode implements Supplier<DyException> {
    ERROR_UNKNOWN(99999900, "未知错误"),
    ERROR_CONFIG_NOT_EXIST(99999901, "小程序配置信息为空"),
    ERROR_NOT_FOUND_RESPONSE_PARSE(99999902, "未找到响应对象解析器"),


    ;
    /**
     * 错误码
     */
    private final Integer code;
    /**
     * 错误信息
     */
    private final String message;


    /**
     * Gets a result.
     *
     * @return a result
     */
    @Override
    public DyException get() {
        return new DyException(this.code, this.message);
    }
}
