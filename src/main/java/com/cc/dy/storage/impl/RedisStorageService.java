package com.cc.dy.storage.impl;

import com.cc.dy.storage.StorageService;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.concurrent.TimeUnit;

/**
 * @author chenChao 2023-09-04 10:53
 */
public class RedisStorageService implements StorageService {
    private final RedisTemplate<String, String> redisTemplate;

    public RedisStorageService(RedisTemplate<String, String> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public String getValue(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    @Override
    public void setValue(String key, String value, int expire, TimeUnit timeUnit) {
        redisTemplate.opsForValue().set(key, value, expire, timeUnit);
    }
}
