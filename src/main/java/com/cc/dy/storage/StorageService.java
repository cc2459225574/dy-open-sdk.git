package com.cc.dy.storage;

import java.util.concurrent.TimeUnit;

/**
 * @author chenChao 2023-09-04 10:01
 */
public interface StorageService {

    String getValue(String key);

    void setValue(String key, String value, int expire, TimeUnit timeUnit);
}
