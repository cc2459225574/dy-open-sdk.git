package com.cc.dy.config;

import lombok.Data;

@Data
public class DyConfig {
    /**
     * 设置抖音开放应用的appId
     */
    private String appId;

    /**
     * 设置抖音开放应用的app secret
     */
    private String clientSecret;

    /**
     * 支付系统秘钥 SALT
     */
    private String salt;

    /**
     * http请求连接超时时间. 单位：秒
     */
    private int httpConnectionTimeout = 5;

    /**
     * http请求数据读取等待时间. 单位：秒
     */
    private int httpTimeout = 10;
}
