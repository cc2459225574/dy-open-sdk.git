package com.cc.dy.json;

import com.cc.dy.api.response.DyBaseResponse;
import com.cc.dy.api.response.DyResponse;

import java.lang.reflect.Field;

public interface JsonSerializer {
    /**
     * 对象转json字符串
     *
     * @param object object
     * @return {@link String}
     */
    String toJson(Object object);

    /**
     * json字符串转对象
     *
     * @param jsonString jsonString
     * @param clazz      clazz
     * @return {@link T}
     */
    <T> T parse(String jsonString, Class<T> clazz);

    /**
     * 抖音开放平台返回结果json字符串取出data转对象
     *
     * @param jsonString jsonString
     * @param clazz      clazz
     * @return {@link T}
     */
    <T extends DyResponse> T parseResponse(String jsonString, Class<T> clazz);

    /**
     * 获取对象字段的别名
     * 通常是用注解来标明别名，所以一般是通过注解获取别名
     *
     * @param field field
     * @return {@link String}
     */
    String getFieldAliasName(Field field);
}
