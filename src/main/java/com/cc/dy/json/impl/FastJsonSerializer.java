package com.cc.dy.json.impl;

import cn.hutool.core.lang.Assert;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cc.dy.api.response.AbstractResponse;
import com.cc.dy.api.response.DyBaseResponse;
import com.cc.dy.api.response.DyResponse;
import com.cc.dy.enums.DyErrorCode;
import com.cc.dy.json.JsonSerializer;
import com.cc.dy.json.parse.ResponseParse;
import com.cc.dy.json.parse.ResponseParseFactory;
import com.google.common.base.Objects;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

/**
 * @author chenChao 2023-09-08 18:52
 */
@Slf4j
public class FastJsonSerializer implements JsonSerializer {
    /**
     * 对象转json字符串
     *
     * @param object object
     * @return {@link String}
     */
    @Override
    public String toJson(Object object) {
        return JSON.toJSONString(object);
    }

    /**
     * json字符串转对象
     *
     * @param jsonString jsonString
     * @param clazz      clazz
     * @return {@link T}
     */
    @Override
    public <T> T parse(String jsonString, Class<T> clazz) {
        return JSON.parseObject(jsonString, clazz);
    }

    /**
     * 抖音开放平台返回结果json字符串取出data转对象
     *
     * @param jsonString jsonString
     * @param clazz      clazz
     * @return {@link T}
     */
    @Override
    public <T extends DyResponse> T parseResponse(String jsonString, Class<T> clazz) {
        ResponseParse responseParse = ResponseParseFactory.getResponseParse(clazz);
        Assert.notNull(responseParse, DyErrorCode.ERROR_NOT_FOUND_RESPONSE_PARSE);
        return responseParse.parse(jsonString, clazz);
    }

    /**
     * 获取对象字段的别名
     * 通常是用注解来标明别名，所以一般是通过注解获取别名
     *
     * @param field field
     * @return {@link String}
     */
    @Override
    public String getFieldAliasName(Field field) {
        return null;
    }
}
