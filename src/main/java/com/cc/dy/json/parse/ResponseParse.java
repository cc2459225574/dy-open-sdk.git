package com.cc.dy.json.parse;

import com.cc.dy.api.response.DyResponse;

/**
 * @author chenChao 2023-09-18 9:57
 */
public interface ResponseParse {

    /**
     * 解析响应对象结构体
     *
     * @param responseText 响应文本
     * @return 响应结果
     */
    <T extends DyResponse> T parse(String responseText, Class<T> clazz);
}
