package com.cc.dy.json.parse.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cc.dy.api.response.DyResponse;
import com.cc.dy.api.response.DyResponseStructV2;
import com.cc.dy.json.parse.ResponseParse;

import java.util.Objects;

/**
 * @author chenChao 2023-09-18 9:58
 */
public class ResponseParseV2 implements ResponseParse {

    /**
     * 解析响应对象结构体
     *
     * @param responseText 响应文本
     * @param clazz
     * @return 响应结果
     */
    @Override
    public <T extends DyResponse> T parse(String responseText, Class<T> clazz) {
        JSONObject jsonObject = JSON.parseObject(responseText);
        T object = jsonObject.getObject("data", clazz);

        DyResponseStructV2 dyResponseStructV2 = (DyResponseStructV2) object;
        dyResponseStructV2.setMessage(jsonObject.getString("message"));

        JSONObject extra = jsonObject.getJSONObject("extra");
        if (Objects.nonNull(extra)) {
            dyResponseStructV2.setLogId(extra.getString("logid"));
            dyResponseStructV2.setNow(extra.getString("now"));
        }
        return object;
    }
}
