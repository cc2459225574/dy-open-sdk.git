package com.cc.dy.json.parse.impl;

import com.cc.dy.api.response.DyResponse;
import com.cc.dy.json.parse.ResponseParse;

/**
 * @author chenChao 2023-09-18 9:58
 */
public class ResponseParseV1 implements ResponseParse {
    /**
     * 解析响应对象结构体
     *
     * @param responseText 响应文本
     * @return 响应结果
     */
    @Override
    public <T extends DyResponse> T parse(String responseText, Class<T> clazz) {
        return null;
    }
}
