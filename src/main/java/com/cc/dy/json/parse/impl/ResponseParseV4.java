package com.cc.dy.json.parse.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cc.dy.api.response.DyResponse;
import com.cc.dy.api.response.DyResponseStructV4;
import com.cc.dy.json.parse.ResponseParse;

/**
 * @author chenChao 2023-09-18 9:58
 */
public class ResponseParseV4 implements ResponseParse {

    /**
     * 解析响应对象结构体
     *
     * @param responseText 响应文本
     * @return 响应结果
     */
    @Override
    public <T extends DyResponse> T parse(String responseText, Class<T> clazz) {
        JSONObject jsonObject = JSON.parseObject(responseText);
        T object = jsonObject.getObject("data", clazz);

        DyResponseStructV4 dyResponseStructV4 = (DyResponseStructV4) object;
        dyResponseStructV4.setErrNo(jsonObject.getLong("err_no"));
        dyResponseStructV4.setErrMsg(jsonObject.getString("err_msg"));
        dyResponseStructV4.setLogId(jsonObject.getString("log_id"));

        return object;
    }
}
