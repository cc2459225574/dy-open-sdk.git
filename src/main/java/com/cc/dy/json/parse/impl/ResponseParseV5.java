package com.cc.dy.json.parse.impl;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cc.dy.api.response.DyResponse;
import com.cc.dy.api.response.DyResponseStructV5;
import com.cc.dy.json.parse.ResponseParse;

/**
 * @author chenChao 2023-09-18 9:58
 */
public class ResponseParseV5 implements ResponseParse {
    /**
     * 解析响应对象结构体
     *
     * @param responseText 响应文本
     * @return 响应结果
     */
    @Override
    public <T extends DyResponse> T parse(String responseText, Class<T> clazz) {
        JSONObject jsonObject = JSON.parseObject(responseText);
        T object = jsonObject.getObject("data", clazz);
        DyResponseStructV5 dyResponseStructV5 = jsonObject.getObject("extra", DyResponseStructV5.class);
        BeanUtil.copyProperties(dyResponseStructV5, object, true);
        return object;
    }
}
