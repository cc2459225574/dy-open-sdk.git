package com.cc.dy.json.parse.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cc.dy.api.response.DyResponse;
import com.cc.dy.api.response.DyResponseStructV3;
import com.cc.dy.json.parse.ResponseParse;

/**
 * @author chenChao 2023-09-18 9:58
 */
public class ResponseParseV3 implements ResponseParse {
    /**
     * 解析响应对象结构体
     *
     * @param responseText 响应文本
     * @return 响应结果
     */
    @Override
    public <T extends DyResponse> T parse(String responseText, Class<T> clazz) {
        JSONObject jsonObject = JSON.parseObject(responseText);

        T object = jsonObject.getObject("data", clazz);
        if (object == null) {
            object = jsonObject.toJavaObject(clazz);
        }

        DyResponseStructV3 dyResponseStructV3 = (DyResponseStructV3) object;

        dyResponseStructV3.setErrNo(jsonObject.getInteger("err_no"));
        dyResponseStructV3.setErrTips(jsonObject.getString("err_tips"));
        return object;
    }
}
