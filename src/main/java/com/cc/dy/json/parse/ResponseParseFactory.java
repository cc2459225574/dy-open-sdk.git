package com.cc.dy.json.parse;

import com.cc.dy.api.response.DyResponseStructV1;
import com.cc.dy.api.response.DyResponseStructV2;
import com.cc.dy.api.response.DyResponseStructV3;
import com.cc.dy.api.response.DyResponseStructV4;
import com.cc.dy.api.response.DyResponseStructV5;
import com.cc.dy.json.parse.impl.ResponseParseV1;
import com.cc.dy.json.parse.impl.ResponseParseV2;
import com.cc.dy.json.parse.impl.ResponseParseV3;
import com.cc.dy.json.parse.impl.ResponseParseV4;
import com.cc.dy.json.parse.impl.ResponseParseV5;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author chenChao 2023-09-18 9:57
 */
public class ResponseParseFactory {
    public static final Map<Class<?>, ResponseParse> RESPONSE_PARSE_MAP = new HashMap<>();

    static {
        RESPONSE_PARSE_MAP.put(DyResponseStructV1.class, new ResponseParseV1());
        RESPONSE_PARSE_MAP.put(DyResponseStructV2.class, new ResponseParseV2());
        RESPONSE_PARSE_MAP.put(DyResponseStructV3.class, new ResponseParseV3());
        RESPONSE_PARSE_MAP.put(DyResponseStructV4.class, new ResponseParseV4());
        RESPONSE_PARSE_MAP.put(DyResponseStructV5.class, new ResponseParseV5());
    }

    public static ResponseParse getResponseParse(Class<?> clazz) {
        ResponseParse responseParse;
        do {
            responseParse = RESPONSE_PARSE_MAP.get(clazz);
            clazz = clazz.getSuperclass();
        } while (Objects.isNull(responseParse) && !Objects.equals(clazz, Object.class));
        return responseParse;
    }
}
