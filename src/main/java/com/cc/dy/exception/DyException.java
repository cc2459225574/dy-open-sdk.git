package com.cc.dy.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author chenChao 2023-09-04 10:11
 */
@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class DyException extends RuntimeException {
    /**
     * 错误码
     */
    private Integer code;
    /**
     * 错误信息
     */
    private String message;
}
